/**
 * Student name: Nguyen Thanh Nhan
 * Student ID: 1512266
 */

grammar MP;

@lexer::header {
from lexererr import *
}

options{
	language=Python3;
}

program: declaration+ EOF;

declaration: varDeclars | funcDeclar | procedureDeclar;

//variable declaration
varDeclars: VAR varDeclar+ ;

varDeclar: ids COLON typeVar SEMI;

ids: ID COMMA ids | ID;

typeVar: primitiveType
	| arrayType;

primitiveType:  INTEGER | BOOLEAN | REAL | STRING;

// ARRADDRESS: [0-9]+;
arrayType: ARRAY LSB INTLIT DOUBLEDOT INTLIT RSB OF primitiveType;

//function declaration
funcDeclar:  FUNCTION ID LB paraList? RB COLON typeVar SEMI varDeclars? compoundStmt; 

paraList: paraDeclar (SEMI paraDeclar)*;

paraDeclar: ids COLON typeVar;

//The compound Statement
compoundStmt: BEGIN (statement)* END;

//procedure declaration
procedureDeclar: PROCEDURE ID LB paraList? RB SEMI varDeclars? compoundStmt;

//The main function
// mainFunc: PROCEDURE MAIN LB RB SEMI compoundStmt;

//character Set

//Comment
TRABLOCKCOMMENT: ('(*' .*? '*)') -> skip;//
BLOCKCOMMENT: ('{' .*? '}') -> skip;//
LINECOMMENT: ('//'(~[\n])*)-> skip;//

//token set

//set case insensitive
fragment A: ('a' | 'A');
fragment B: ('b' | 'B');
fragment C: ('c' | 'C');
fragment D: ('d' | 'D');
fragment E: ('e' | 'E');
fragment F: ('f' | 'F');
fragment G: ('g' | 'G');
fragment H: ('h' | 'H');
fragment I: ('i' | 'I');
fragment K: ('k' | 'K');
fragment L: ('l' | 'L');
fragment M: ('m' | 'M');
fragment N: ('n' | 'N');
fragment O: ('o' | 'O');
fragment P: ('p' | 'P');
fragment R: ('r' | 'R');
fragment S: ('s' | 'S');
fragment T: ('t' | 'T');
fragment U: ('u' | 'U');
fragment V: ('v' | 'V');
fragment W: ('w' | 'W');
fragment Y: ('y' | 'Y');

//keywords
BREAK: B R E A K;
CONTINUE: C O N T I N U E;
FOR: F O R;
TO: T O;
DOWNTO: D O W N T O;
DO: D O;
IF: I F;
THEN: T H E N;
ELSE: E L S E;
RETURN: R E T U R N;
WHILE: W H I L E;
BEGIN: B E G I N;
END: E N D;
FUNCTION: F U N C T I O N;
PROCEDURE: P R O C E D U R E;
VAR: V A R;
TRUE: T R U E;
FALSE: F A L S E;
ARRAY: A R R A Y;
OF: O F;
WITH: W I T H;
REAL: R E A L;
BOOLEAN: B O O L E A N;
INTEGER:I N T E G E R;
STRING: S T R I N G;
// MAIN: M A I N;

//operators
ADD: '+';
SUB: '-';
MUL: '*';
DIV: '/';
NOT: N O T ; //not
MOD: M O D; //mod
OR: O R; //or
AND: A N D; //and
NEQUAL: '<>';
EQUAL: '=';
ASSIGN: ':=';
LETHANE: '<=';
GRETHANE: '>=';
LETHAN: '<';
GRETHAN: '>';
INDIV: D I V ;//div

// //boolean literal
// BOOLEANLIT: TRUE | FALSE;




//separators
LSB: '[';
RSB: ']';

COLON: ':';

LB: '(' ;
RB: ')' ;

SEMI: ';' ;

DOUBLEDOT: '..';

COMMA: ',';


//Literals
//interger literal
INTLIT: [0-9]+;

//float-point literal
fragment NUMBER: [0-9]+;
fragment DEPT: '-';
fragment EXP: [eE];
fragment DOT: '.';
FLOATLIT: (NUMBER DOT NUMBER) | (NUMBER DOT) | (DOT NUMBER)
    | (NUMBER DOT NUMBER? EXP DEPT? NUMBER)
    | (DOT? NUMBER EXP DEPT? NUMBER);


//string literal
fragment ESCAPE: '\\'[bfrnt\\'"] ;
STRINGLIT:'"' (~["\\\n]|ESCAPE)* '"' {self.text =  self.text[1:len(self.text) - 1]};



//expression
expression: expression (AND THEN | OR ELSE) exp1
	| exp1;
exp1: exp2 (EQUAL | NEQUAL | LETHAN | LETHANE | GRETHAN | GRETHANE) exp2
	| exp2;
exp2: exp2 (ADD | SUB | OR) exp3
	| exp3;
exp3: exp3 (DIV | MUL | INDIV | MOD | AND) exp4
	| exp4;
exp4: (SUB | NOT) exp4
	| exp5;

// //index expression
exp5: indexExpr | exp6;

exp6: invocationExp 
	| LB expression RB
	| ID
	| FLOATLIT
	| STRINGLIT
	| INTLIT
	| TRUE | FALSE;

//Invocation Expression
invocationExp: ID LB exp_list? RB;
exp_list: expression (COMMA expression)*;

//index expression
indexExpr: exp6 LSB expression RSB; // (invocationExp | ID | STRINGLIT)

//statement
//Assignment Statement
assignSttm: lhs expression SEMI; 

lhs: lhsItem ASSIGN lhs | lhsItem ASSIGN;

lhsItem: ID | indexExpr;

//The if Statement
ifStatement: IF expression THEN lstStatement ELSE lstStatement 
            | IF expression THEN lstStatement;

//The while Statement
whileStatement: WHILE expression DO lstStatement;

lstStatement: statement lstStatement*;

//The for Statement
forStatement: FOR ID ASSIGN expression (TO | DOWNTO) expression DO lstStatement;

//The break Statement
breakStatement: BREAK SEMI;

//The continue Statement
continueStatement: CONTINUE SEMI;

//The return Statement
returnStatement: RETURN expression? SEMI;

//The compound Statement => was defined above

//The with statement
withSttm: WITH (ID (COMMA ID)* COLON typeVar SEMI)+ DO lstStatement;

//Call statement
callFunc: invocationExp SEMI;

//statement
statement: assignSttm | ifStatement
	| whileStatement | forStatement
	| breakStatement | continueStatement
	| returnStatement 
	| withSttm | callFunc | compoundStmt;

//identifiers
ID: [_a-zA-Z][_a-zA-Z0-9]*;

WS : [ \f\t\r\n]+ -> skip ; // skip spaces, tabs, newlines

UNCLOSE_STRING: '"'(ESCAPE | ~[\r\n"\\EOF])*{ raise UncloseString(self.text[1:]) };

ERROR_CHAR: .{raise ErrorToken(self.text)};

fragment IES: '\\'(~[bnfrt\\'"]) ; // Illegal escape
ILLEGAL_ESCAPE: '"' (~["\\\n]|ESCAPE)*IES {raise IllegalEscape(self.text[1:])};