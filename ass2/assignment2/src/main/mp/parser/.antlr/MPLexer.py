# Generated from d:\181\ppl\ass2\assignment2\src\main\mp\parser\MP.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


from lexererr import *


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2>")
        buf.write("\u0239\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:")
        buf.write("\4;\t;\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\t")
        buf.write("C\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I\tI\4J\tJ\4K\tK\4L\t")
        buf.write("L\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT\4U\t")
        buf.write("U\4V\tV\4W\tW\4X\tX\4Y\tY\3\2\3\2\3\2\3\2\7\2\u00b8\n")
        buf.write("\2\f\2\16\2\u00bb\13\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\7\3")
        buf.write("\u00c4\n\3\f\3\16\3\u00c7\13\3\3\3\3\3\3\3\3\3\3\4\3\4")
        buf.write("\3\4\3\4\7\4\u00d1\n\4\f\4\16\4\u00d4\13\4\3\4\3\4\3\5")
        buf.write("\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13")
        buf.write("\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3")
        buf.write("\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\3\26\3\26")
        buf.write("\3\27\3\27\3\30\3\30\3\31\3\31\3\32\3\32\3\33\3\33\3\33")
        buf.write("\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34")
        buf.write("\3\34\3\35\3\35\3\35\3\35\3\36\3\36\3\36\3\37\3\37\3\37")
        buf.write("\3\37\3\37\3\37\3\37\3 \3 \3 \3!\3!\3!\3\"\3\"\3\"\3\"")
        buf.write("\3\"\3#\3#\3#\3#\3#\3$\3$\3$\3$\3$\3$\3$\3%\3%\3%\3%\3")
        buf.write("%\3%\3&\3&\3&\3&\3&\3&\3\'\3\'\3\'\3\'\3(\3(\3(\3(\3(")
        buf.write("\3(\3(\3(\3(\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3*\3*\3*\3")
        buf.write("*\3+\3+\3+\3+\3+\3,\3,\3,\3,\3,\3,\3-\3-\3-\3-\3-\3-\3")
        buf.write(".\3.\3.\3/\3/\3/\3/\3/\3\60\3\60\3\60\3\60\3\60\3\61\3")
        buf.write("\61\3\61\3\61\3\61\3\61\3\61\3\61\3\62\3\62\3\62\3\62")
        buf.write("\3\62\3\62\3\62\3\62\3\63\3\63\3\63\3\63\3\63\3\63\3\63")
        buf.write("\3\64\3\64\3\65\3\65\3\66\3\66\3\67\3\67\38\38\38\38\3")
        buf.write("9\39\39\39\3:\3:\3:\3;\3;\3;\3;\3<\3<\3<\3=\3=\3>\3>\3")
        buf.write(">\3?\3?\3?\3@\3@\3@\3A\3A\3B\3B\3C\3C\3C\3C\3D\3D\3E\3")
        buf.write("E\3F\3F\3G\3G\3H\3H\3I\3I\3J\3J\3J\3K\3K\3L\6L\u01d3\n")
        buf.write("L\rL\16L\u01d4\3M\6M\u01d8\nM\rM\16M\u01d9\3N\3N\3O\3")
        buf.write("O\3P\3P\3Q\3Q\3Q\3Q\3Q\3Q\3Q\3Q\3Q\3Q\3Q\3Q\3Q\5Q\u01ef")
        buf.write("\nQ\3Q\3Q\5Q\u01f3\nQ\3Q\3Q\3Q\5Q\u01f8\nQ\3Q\3Q\3Q\5")
        buf.write("Q\u01fd\nQ\3Q\3Q\5Q\u0201\nQ\3R\3R\3R\3S\3S\3S\7S\u0209")
        buf.write("\nS\fS\16S\u020c\13S\3S\3S\3S\3T\3T\7T\u0213\nT\fT\16")
        buf.write("T\u0216\13T\3U\6U\u0219\nU\rU\16U\u021a\3U\3U\3V\3V\3")
        buf.write("V\7V\u0222\nV\fV\16V\u0225\13V\3V\3V\3W\3W\3W\3X\3X\3")
        buf.write("X\3Y\3Y\3Y\7Y\u0232\nY\fY\16Y\u0235\13Y\3Y\3Y\3Y\4\u00b9")
        buf.write("\u00c5\2Z\3\3\5\4\7\5\t\2\13\2\r\2\17\2\21\2\23\2\25\2")
        buf.write("\27\2\31\2\33\2\35\2\37\2!\2#\2%\2\'\2)\2+\2-\2/\2\61")
        buf.write("\2\63\2\65\6\67\79\b;\t=\n?\13A\fC\rE\16G\17I\20K\21M")
        buf.write("\22O\23Q\24S\25U\26W\27Y\30[\31]\32_\33a\34c\35e\36g\37")
        buf.write("i k!m\"o#q$s%u&w\'y({)}*\177+\u0081,\u0083-\u0085.\u0087")
        buf.write("/\u0089\60\u008b\61\u008d\62\u008f\63\u0091\64\u0093\65")
        buf.write("\u0095\66\u0097\67\u0099\2\u009b\2\u009d\2\u009f\2\u00a1")
        buf.write("8\u00a3\2\u00a59\u00a7:\u00a9;\u00ab<\u00ad=\u00af\2\u00b1")
        buf.write(">\3\2 \3\2\f\f\4\2CCcc\4\2DDdd\4\2EEee\4\2FFff\4\2GGg")
        buf.write("g\4\2HHhh\4\2IIii\4\2JJjj\4\2KKkk\4\2MMmm\4\2NNnn\4\2")
        buf.write("OOoo\4\2PPpp\4\2QQqq\4\2RRrr\4\2TTtt\4\2UUuu\4\2VVvv\4")
        buf.write("\2WWww\4\2XXxx\4\2YYyy\4\2[[{{\3\2\62;\n\2$$))^^ddhhp")
        buf.write("pttvv\5\2\f\f$$^^\5\2C\\aac|\6\2\62;C\\aac|\5\2\13\f\16")
        buf.write("\17\"\"\b\2\f\f\17\17$$GHQQ^^\2\u0231\2\3\3\2\2\2\2\5")
        buf.write("\3\2\2\2\2\7\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2")
        buf.write("\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3")
        buf.write("\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M")
        buf.write("\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2")
        buf.write("W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2")
        buf.write("\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2")
        buf.write("\2\2k\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2\2\2s\3\2")
        buf.write("\2\2\2u\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\2{\3\2\2\2\2}\3")
        buf.write("\2\2\2\2\177\3\2\2\2\2\u0081\3\2\2\2\2\u0083\3\2\2\2\2")
        buf.write("\u0085\3\2\2\2\2\u0087\3\2\2\2\2\u0089\3\2\2\2\2\u008b")
        buf.write("\3\2\2\2\2\u008d\3\2\2\2\2\u008f\3\2\2\2\2\u0091\3\2\2")
        buf.write("\2\2\u0093\3\2\2\2\2\u0095\3\2\2\2\2\u0097\3\2\2\2\2\u00a1")
        buf.write("\3\2\2\2\2\u00a5\3\2\2\2\2\u00a7\3\2\2\2\2\u00a9\3\2\2")
        buf.write("\2\2\u00ab\3\2\2\2\2\u00ad\3\2\2\2\2\u00b1\3\2\2\2\3\u00b3")
        buf.write("\3\2\2\2\5\u00c1\3\2\2\2\7\u00cc\3\2\2\2\t\u00d7\3\2\2")
        buf.write("\2\13\u00d9\3\2\2\2\r\u00db\3\2\2\2\17\u00dd\3\2\2\2\21")
        buf.write("\u00df\3\2\2\2\23\u00e1\3\2\2\2\25\u00e3\3\2\2\2\27\u00e5")
        buf.write("\3\2\2\2\31\u00e7\3\2\2\2\33\u00e9\3\2\2\2\35\u00eb\3")
        buf.write("\2\2\2\37\u00ed\3\2\2\2!\u00ef\3\2\2\2#\u00f1\3\2\2\2")
        buf.write("%\u00f3\3\2\2\2\'\u00f5\3\2\2\2)\u00f7\3\2\2\2+\u00f9")
        buf.write("\3\2\2\2-\u00fb\3\2\2\2/\u00fd\3\2\2\2\61\u00ff\3\2\2")
        buf.write("\2\63\u0101\3\2\2\2\65\u0103\3\2\2\2\67\u0109\3\2\2\2")
        buf.write("9\u0112\3\2\2\2;\u0116\3\2\2\2=\u0119\3\2\2\2?\u0120\3")
        buf.write("\2\2\2A\u0123\3\2\2\2C\u0126\3\2\2\2E\u012b\3\2\2\2G\u0130")
        buf.write("\3\2\2\2I\u0137\3\2\2\2K\u013d\3\2\2\2M\u0143\3\2\2\2")
        buf.write("O\u0147\3\2\2\2Q\u0150\3\2\2\2S\u015a\3\2\2\2U\u015e\3")
        buf.write("\2\2\2W\u0163\3\2\2\2Y\u0169\3\2\2\2[\u016f\3\2\2\2]\u0172")
        buf.write("\3\2\2\2_\u0177\3\2\2\2a\u017c\3\2\2\2c\u0184\3\2\2\2")
        buf.write("e\u018c\3\2\2\2g\u0193\3\2\2\2i\u0195\3\2\2\2k\u0197\3")
        buf.write("\2\2\2m\u0199\3\2\2\2o\u019b\3\2\2\2q\u019f\3\2\2\2s\u01a3")
        buf.write("\3\2\2\2u\u01a6\3\2\2\2w\u01aa\3\2\2\2y\u01ad\3\2\2\2")
        buf.write("{\u01af\3\2\2\2}\u01b2\3\2\2\2\177\u01b5\3\2\2\2\u0081")
        buf.write("\u01b8\3\2\2\2\u0083\u01ba\3\2\2\2\u0085\u01bc\3\2\2\2")
        buf.write("\u0087\u01c0\3\2\2\2\u0089\u01c2\3\2\2\2\u008b\u01c4\3")
        buf.write("\2\2\2\u008d\u01c6\3\2\2\2\u008f\u01c8\3\2\2\2\u0091\u01ca")
        buf.write("\3\2\2\2\u0093\u01cc\3\2\2\2\u0095\u01cf\3\2\2\2\u0097")
        buf.write("\u01d2\3\2\2\2\u0099\u01d7\3\2\2\2\u009b\u01db\3\2\2\2")
        buf.write("\u009d\u01dd\3\2\2\2\u009f\u01df\3\2\2\2\u00a1\u0200\3")
        buf.write("\2\2\2\u00a3\u0202\3\2\2\2\u00a5\u0205\3\2\2\2\u00a7\u0210")
        buf.write("\3\2\2\2\u00a9\u0218\3\2\2\2\u00ab\u021e\3\2\2\2\u00ad")
        buf.write("\u0228\3\2\2\2\u00af\u022b\3\2\2\2\u00b1\u022e\3\2\2\2")
        buf.write("\u00b3\u00b4\7*\2\2\u00b4\u00b5\7,\2\2\u00b5\u00b9\3\2")
        buf.write("\2\2\u00b6\u00b8\13\2\2\2\u00b7\u00b6\3\2\2\2\u00b8\u00bb")
        buf.write("\3\2\2\2\u00b9\u00ba\3\2\2\2\u00b9\u00b7\3\2\2\2\u00ba")
        buf.write("\u00bc\3\2\2\2\u00bb\u00b9\3\2\2\2\u00bc\u00bd\7,\2\2")
        buf.write("\u00bd\u00be\7+\2\2\u00be\u00bf\3\2\2\2\u00bf\u00c0\b")
        buf.write("\2\2\2\u00c0\4\3\2\2\2\u00c1\u00c5\7}\2\2\u00c2\u00c4")
        buf.write("\13\2\2\2\u00c3\u00c2\3\2\2\2\u00c4\u00c7\3\2\2\2\u00c5")
        buf.write("\u00c6\3\2\2\2\u00c5\u00c3\3\2\2\2\u00c6\u00c8\3\2\2\2")
        buf.write("\u00c7\u00c5\3\2\2\2\u00c8\u00c9\7\177\2\2\u00c9\u00ca")
        buf.write("\3\2\2\2\u00ca\u00cb\b\3\2\2\u00cb\6\3\2\2\2\u00cc\u00cd")
        buf.write("\7\61\2\2\u00cd\u00ce\7\61\2\2\u00ce\u00d2\3\2\2\2\u00cf")
        buf.write("\u00d1\n\2\2\2\u00d0\u00cf\3\2\2\2\u00d1\u00d4\3\2\2\2")
        buf.write("\u00d2\u00d0\3\2\2\2\u00d2\u00d3\3\2\2\2\u00d3\u00d5\3")
        buf.write("\2\2\2\u00d4\u00d2\3\2\2\2\u00d5\u00d6\b\4\2\2\u00d6\b")
        buf.write("\3\2\2\2\u00d7\u00d8\t\3\2\2\u00d8\n\3\2\2\2\u00d9\u00da")
        buf.write("\t\4\2\2\u00da\f\3\2\2\2\u00db\u00dc\t\5\2\2\u00dc\16")
        buf.write("\3\2\2\2\u00dd\u00de\t\6\2\2\u00de\20\3\2\2\2\u00df\u00e0")
        buf.write("\t\7\2\2\u00e0\22\3\2\2\2\u00e1\u00e2\t\b\2\2\u00e2\24")
        buf.write("\3\2\2\2\u00e3\u00e4\t\t\2\2\u00e4\26\3\2\2\2\u00e5\u00e6")
        buf.write("\t\n\2\2\u00e6\30\3\2\2\2\u00e7\u00e8\t\13\2\2\u00e8\32")
        buf.write("\3\2\2\2\u00e9\u00ea\t\f\2\2\u00ea\34\3\2\2\2\u00eb\u00ec")
        buf.write("\t\r\2\2\u00ec\36\3\2\2\2\u00ed\u00ee\t\16\2\2\u00ee ")
        buf.write("\3\2\2\2\u00ef\u00f0\t\17\2\2\u00f0\"\3\2\2\2\u00f1\u00f2")
        buf.write("\t\20\2\2\u00f2$\3\2\2\2\u00f3\u00f4\t\21\2\2\u00f4&\3")
        buf.write("\2\2\2\u00f5\u00f6\t\22\2\2\u00f6(\3\2\2\2\u00f7\u00f8")
        buf.write("\t\23\2\2\u00f8*\3\2\2\2\u00f9\u00fa\t\24\2\2\u00fa,\3")
        buf.write("\2\2\2\u00fb\u00fc\t\25\2\2\u00fc.\3\2\2\2\u00fd\u00fe")
        buf.write("\t\26\2\2\u00fe\60\3\2\2\2\u00ff\u0100\t\27\2\2\u0100")
        buf.write("\62\3\2\2\2\u0101\u0102\t\30\2\2\u0102\64\3\2\2\2\u0103")
        buf.write("\u0104\5\13\6\2\u0104\u0105\5\'\24\2\u0105\u0106\5\21")
        buf.write("\t\2\u0106\u0107\5\t\5\2\u0107\u0108\5\33\16\2\u0108\66")
        buf.write("\3\2\2\2\u0109\u010a\5\r\7\2\u010a\u010b\5#\22\2\u010b")
        buf.write("\u010c\5!\21\2\u010c\u010d\5+\26\2\u010d\u010e\5\31\r")
        buf.write("\2\u010e\u010f\5!\21\2\u010f\u0110\5-\27\2\u0110\u0111")
        buf.write("\5\21\t\2\u01118\3\2\2\2\u0112\u0113\5\23\n\2\u0113\u0114")
        buf.write("\5#\22\2\u0114\u0115\5\'\24\2\u0115:\3\2\2\2\u0116\u0117")
        buf.write("\5+\26\2\u0117\u0118\5#\22\2\u0118<\3\2\2\2\u0119\u011a")
        buf.write("\5\17\b\2\u011a\u011b\5#\22\2\u011b\u011c\5\61\31\2\u011c")
        buf.write("\u011d\5!\21\2\u011d\u011e\5+\26\2\u011e\u011f\5#\22\2")
        buf.write("\u011f>\3\2\2\2\u0120\u0121\5\17\b\2\u0121\u0122\5#\22")
        buf.write("\2\u0122@\3\2\2\2\u0123\u0124\5\31\r\2\u0124\u0125\5\23")
        buf.write("\n\2\u0125B\3\2\2\2\u0126\u0127\5+\26\2\u0127\u0128\5")
        buf.write("\27\f\2\u0128\u0129\5\21\t\2\u0129\u012a\5!\21\2\u012a")
        buf.write("D\3\2\2\2\u012b\u012c\5\21\t\2\u012c\u012d\5\35\17\2\u012d")
        buf.write("\u012e\5)\25\2\u012e\u012f\5\21\t\2\u012fF\3\2\2\2\u0130")
        buf.write("\u0131\5\'\24\2\u0131\u0132\5\21\t\2\u0132\u0133\5+\26")
        buf.write("\2\u0133\u0134\5-\27\2\u0134\u0135\5\'\24\2\u0135\u0136")
        buf.write("\5!\21\2\u0136H\3\2\2\2\u0137\u0138\5\61\31\2\u0138\u0139")
        buf.write("\5\27\f\2\u0139\u013a\5\31\r\2\u013a\u013b\5\35\17\2\u013b")
        buf.write("\u013c\5\21\t\2\u013cJ\3\2\2\2\u013d\u013e\5\13\6\2\u013e")
        buf.write("\u013f\5\21\t\2\u013f\u0140\5\25\13\2\u0140\u0141\5\31")
        buf.write("\r\2\u0141\u0142\5!\21\2\u0142L\3\2\2\2\u0143\u0144\5")
        buf.write("\21\t\2\u0144\u0145\5!\21\2\u0145\u0146\5\17\b\2\u0146")
        buf.write("N\3\2\2\2\u0147\u0148\5\23\n\2\u0148\u0149\5-\27\2\u0149")
        buf.write("\u014a\5!\21\2\u014a\u014b\5\r\7\2\u014b\u014c\5+\26\2")
        buf.write("\u014c\u014d\5\31\r\2\u014d\u014e\5#\22\2\u014e\u014f")
        buf.write("\5!\21\2\u014fP\3\2\2\2\u0150\u0151\5%\23\2\u0151\u0152")
        buf.write("\5\'\24\2\u0152\u0153\5#\22\2\u0153\u0154\5\r\7\2\u0154")
        buf.write("\u0155\5\21\t\2\u0155\u0156\5\17\b\2\u0156\u0157\5-\27")
        buf.write("\2\u0157\u0158\5\'\24\2\u0158\u0159\5\21\t\2\u0159R\3")
        buf.write("\2\2\2\u015a\u015b\5/\30\2\u015b\u015c\5\t\5\2\u015c\u015d")
        buf.write("\5\'\24\2\u015dT\3\2\2\2\u015e\u015f\5+\26\2\u015f\u0160")
        buf.write("\5\'\24\2\u0160\u0161\5-\27\2\u0161\u0162\5\21\t\2\u0162")
        buf.write("V\3\2\2\2\u0163\u0164\5\23\n\2\u0164\u0165\5\t\5\2\u0165")
        buf.write("\u0166\5\35\17\2\u0166\u0167\5)\25\2\u0167\u0168\5\21")
        buf.write("\t\2\u0168X\3\2\2\2\u0169\u016a\5\t\5\2\u016a\u016b\5")
        buf.write("\'\24\2\u016b\u016c\5\'\24\2\u016c\u016d\5\t\5\2\u016d")
        buf.write("\u016e\5\63\32\2\u016eZ\3\2\2\2\u016f\u0170\5#\22\2\u0170")
        buf.write("\u0171\5\23\n\2\u0171\\\3\2\2\2\u0172\u0173\5\61\31\2")
        buf.write("\u0173\u0174\5\31\r\2\u0174\u0175\5+\26\2\u0175\u0176")
        buf.write("\5\27\f\2\u0176^\3\2\2\2\u0177\u0178\5\'\24\2\u0178\u0179")
        buf.write("\5\21\t\2\u0179\u017a\5\t\5\2\u017a\u017b\5\35\17\2\u017b")
        buf.write("`\3\2\2\2\u017c\u017d\5\13\6\2\u017d\u017e\5#\22\2\u017e")
        buf.write("\u017f\5#\22\2\u017f\u0180\5\35\17\2\u0180\u0181\5\21")
        buf.write("\t\2\u0181\u0182\5\t\5\2\u0182\u0183\5!\21\2\u0183b\3")
        buf.write("\2\2\2\u0184\u0185\5\31\r\2\u0185\u0186\5!\21\2\u0186")
        buf.write("\u0187\5+\26\2\u0187\u0188\5\21\t\2\u0188\u0189\5\25\13")
        buf.write("\2\u0189\u018a\5\21\t\2\u018a\u018b\5\'\24\2\u018bd\3")
        buf.write("\2\2\2\u018c\u018d\5)\25\2\u018d\u018e\5+\26\2\u018e\u018f")
        buf.write("\5\'\24\2\u018f\u0190\5\31\r\2\u0190\u0191\5!\21\2\u0191")
        buf.write("\u0192\5\25\13\2\u0192f\3\2\2\2\u0193\u0194\7-\2\2\u0194")
        buf.write("h\3\2\2\2\u0195\u0196\7/\2\2\u0196j\3\2\2\2\u0197\u0198")
        buf.write("\7,\2\2\u0198l\3\2\2\2\u0199\u019a\7\61\2\2\u019an\3\2")
        buf.write("\2\2\u019b\u019c\5!\21\2\u019c\u019d\5#\22\2\u019d\u019e")
        buf.write("\5+\26\2\u019ep\3\2\2\2\u019f\u01a0\5\37\20\2\u01a0\u01a1")
        buf.write("\5#\22\2\u01a1\u01a2\5\17\b\2\u01a2r\3\2\2\2\u01a3\u01a4")
        buf.write("\5#\22\2\u01a4\u01a5\5\'\24\2\u01a5t\3\2\2\2\u01a6\u01a7")
        buf.write("\5\t\5\2\u01a7\u01a8\5!\21\2\u01a8\u01a9\5\17\b\2\u01a9")
        buf.write("v\3\2\2\2\u01aa\u01ab\7>\2\2\u01ab\u01ac\7@\2\2\u01ac")
        buf.write("x\3\2\2\2\u01ad\u01ae\7?\2\2\u01aez\3\2\2\2\u01af\u01b0")
        buf.write("\7<\2\2\u01b0\u01b1\7?\2\2\u01b1|\3\2\2\2\u01b2\u01b3")
        buf.write("\7>\2\2\u01b3\u01b4\7?\2\2\u01b4~\3\2\2\2\u01b5\u01b6")
        buf.write("\7@\2\2\u01b6\u01b7\7?\2\2\u01b7\u0080\3\2\2\2\u01b8\u01b9")
        buf.write("\7>\2\2\u01b9\u0082\3\2\2\2\u01ba\u01bb\7@\2\2\u01bb\u0084")
        buf.write("\3\2\2\2\u01bc\u01bd\5\17\b\2\u01bd\u01be\5\31\r\2\u01be")
        buf.write("\u01bf\5/\30\2\u01bf\u0086\3\2\2\2\u01c0\u01c1\7]\2\2")
        buf.write("\u01c1\u0088\3\2\2\2\u01c2\u01c3\7_\2\2\u01c3\u008a\3")
        buf.write("\2\2\2\u01c4\u01c5\7<\2\2\u01c5\u008c\3\2\2\2\u01c6\u01c7")
        buf.write("\7*\2\2\u01c7\u008e\3\2\2\2\u01c8\u01c9\7+\2\2\u01c9\u0090")
        buf.write("\3\2\2\2\u01ca\u01cb\7=\2\2\u01cb\u0092\3\2\2\2\u01cc")
        buf.write("\u01cd\7\60\2\2\u01cd\u01ce\7\60\2\2\u01ce\u0094\3\2\2")
        buf.write("\2\u01cf\u01d0\7.\2\2\u01d0\u0096\3\2\2\2\u01d1\u01d3")
        buf.write("\t\31\2\2\u01d2\u01d1\3\2\2\2\u01d3\u01d4\3\2\2\2\u01d4")
        buf.write("\u01d2\3\2\2\2\u01d4\u01d5\3\2\2\2\u01d5\u0098\3\2\2\2")
        buf.write("\u01d6\u01d8\t\31\2\2\u01d7\u01d6\3\2\2\2\u01d8\u01d9")
        buf.write("\3\2\2\2\u01d9\u01d7\3\2\2\2\u01d9\u01da\3\2\2\2\u01da")
        buf.write("\u009a\3\2\2\2\u01db\u01dc\7/\2\2\u01dc\u009c\3\2\2\2")
        buf.write("\u01dd\u01de\t\7\2\2\u01de\u009e\3\2\2\2\u01df\u01e0\7")
        buf.write("\60\2\2\u01e0\u00a0\3\2\2\2\u01e1\u01e2\5\u0099M\2\u01e2")
        buf.write("\u01e3\5\u009fP\2\u01e3\u01e4\5\u0099M\2\u01e4\u0201\3")
        buf.write("\2\2\2\u01e5\u01e6\5\u0099M\2\u01e6\u01e7\5\u009fP\2\u01e7")
        buf.write("\u0201\3\2\2\2\u01e8\u01e9\5\u009fP\2\u01e9\u01ea\5\u0099")
        buf.write("M\2\u01ea\u0201\3\2\2\2\u01eb\u01ec\5\u0099M\2\u01ec\u01ee")
        buf.write("\5\u009fP\2\u01ed\u01ef\5\u0099M\2\u01ee\u01ed\3\2\2\2")
        buf.write("\u01ee\u01ef\3\2\2\2\u01ef\u01f0\3\2\2\2\u01f0\u01f2\5")
        buf.write("\u009dO\2\u01f1\u01f3\5\u009bN\2\u01f2\u01f1\3\2\2\2\u01f2")
        buf.write("\u01f3\3\2\2\2\u01f3\u01f4\3\2\2\2\u01f4\u01f5\5\u0099")
        buf.write("M\2\u01f5\u0201\3\2\2\2\u01f6\u01f8\5\u009fP\2\u01f7\u01f6")
        buf.write("\3\2\2\2\u01f7\u01f8\3\2\2\2\u01f8\u01f9\3\2\2\2\u01f9")
        buf.write("\u01fa\5\u0099M\2\u01fa\u01fc\5\u009dO\2\u01fb\u01fd\5")
        buf.write("\u009bN\2\u01fc\u01fb\3\2\2\2\u01fc\u01fd\3\2\2\2\u01fd")
        buf.write("\u01fe\3\2\2\2\u01fe\u01ff\5\u0099M\2\u01ff\u0201\3\2")
        buf.write("\2\2\u0200\u01e1\3\2\2\2\u0200\u01e5\3\2\2\2\u0200\u01e8")
        buf.write("\3\2\2\2\u0200\u01eb\3\2\2\2\u0200\u01f7\3\2\2\2\u0201")
        buf.write("\u00a2\3\2\2\2\u0202\u0203\7^\2\2\u0203\u0204\t\32\2\2")
        buf.write("\u0204\u00a4\3\2\2\2\u0205\u020a\7$\2\2\u0206\u0209\n")
        buf.write("\33\2\2\u0207\u0209\5\u00a3R\2\u0208\u0206\3\2\2\2\u0208")
        buf.write("\u0207\3\2\2\2\u0209\u020c\3\2\2\2\u020a\u0208\3\2\2\2")
        buf.write("\u020a\u020b\3\2\2\2\u020b\u020d\3\2\2\2\u020c\u020a\3")
        buf.write("\2\2\2\u020d\u020e\7$\2\2\u020e\u020f\bS\3\2\u020f\u00a6")
        buf.write("\3\2\2\2\u0210\u0214\t\34\2\2\u0211\u0213\t\35\2\2\u0212")
        buf.write("\u0211\3\2\2\2\u0213\u0216\3\2\2\2\u0214\u0212\3\2\2\2")
        buf.write("\u0214\u0215\3\2\2\2\u0215\u00a8\3\2\2\2\u0216\u0214\3")
        buf.write("\2\2\2\u0217\u0219\t\36\2\2\u0218\u0217\3\2\2\2\u0219")
        buf.write("\u021a\3\2\2\2\u021a\u0218\3\2\2\2\u021a\u021b\3\2\2\2")
        buf.write("\u021b\u021c\3\2\2\2\u021c\u021d\bU\2\2\u021d\u00aa\3")
        buf.write("\2\2\2\u021e\u0223\7$\2\2\u021f\u0222\5\u00a3R\2\u0220")
        buf.write("\u0222\n\37\2\2\u0221\u021f\3\2\2\2\u0221\u0220\3\2\2")
        buf.write("\2\u0222\u0225\3\2\2\2\u0223\u0221\3\2\2\2\u0223\u0224")
        buf.write("\3\2\2\2\u0224\u0226\3\2\2\2\u0225\u0223\3\2\2\2\u0226")
        buf.write("\u0227\bV\4\2\u0227\u00ac\3\2\2\2\u0228\u0229\13\2\2\2")
        buf.write("\u0229\u022a\bW\5\2\u022a\u00ae\3\2\2\2\u022b\u022c\7")
        buf.write("^\2\2\u022c\u022d\n\32\2\2\u022d\u00b0\3\2\2\2\u022e\u0233")
        buf.write("\7$\2\2\u022f\u0232\n\33\2\2\u0230\u0232\5\u00a3R\2\u0231")
        buf.write("\u022f\3\2\2\2\u0231\u0230\3\2\2\2\u0232\u0235\3\2\2\2")
        buf.write("\u0233\u0231\3\2\2\2\u0233\u0234\3\2\2\2\u0234\u0236\3")
        buf.write("\2\2\2\u0235\u0233\3\2\2\2\u0236\u0237\5\u00afX\2\u0237")
        buf.write("\u0238\bY\6\2\u0238\u00b2\3\2\2\2\25\2\u00b9\u00c5\u00d2")
        buf.write("\u01d4\u01d9\u01ee\u01f2\u01f7\u01fc\u0200\u0208\u020a")
        buf.write("\u0214\u021a\u0221\u0223\u0231\u0233\7\b\2\2\3S\2\3V\3")
        buf.write("\3W\4\3Y\5")
        return buf.getvalue()


class MPLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    TRABLOCKCOMMENT = 1
    BLOCKCOMMENT = 2
    LINECOMMENT = 3
    BREAK = 4
    CONTINUE = 5
    FOR = 6
    TO = 7
    DOWNTO = 8
    DO = 9
    IF = 10
    THEN = 11
    ELSE = 12
    RETURN = 13
    WHILE = 14
    BEGIN = 15
    END = 16
    FUNCTION = 17
    PROCEDURE = 18
    VAR = 19
    TRUE = 20
    FALSE = 21
    ARRAY = 22
    OF = 23
    WITH = 24
    REAL = 25
    BOOLEAN = 26
    INTEGER = 27
    STRING = 28
    ADD = 29
    SUB = 30
    MUL = 31
    DIV = 32
    NOT = 33
    MOD = 34
    OR = 35
    AND = 36
    NEQUAL = 37
    EQUAL = 38
    ASSIGN = 39
    LETHANE = 40
    GRETHANE = 41
    LETHAN = 42
    GRETHAN = 43
    INDIV = 44
    LSB = 45
    RSB = 46
    COLON = 47
    LB = 48
    RB = 49
    SEMI = 50
    DOUBLEDOT = 51
    COMMA = 52
    INTLIT = 53
    FLOATLIT = 54
    STRINGLIT = 55
    ID = 56
    WS = 57
    UNCLOSE_STRING = 58
    ERROR_CHAR = 59
    ILLEGAL_ESCAPE = 60

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'+'", "'-'", "'*'", "'/'", "'<>'", "'='", "':='", "'<='", "'>='", 
            "'<'", "'>'", "'['", "']'", "':'", "'('", "')'", "';'", "'..'", 
            "','" ]

    symbolicNames = [ "<INVALID>",
            "TRABLOCKCOMMENT", "BLOCKCOMMENT", "LINECOMMENT", "BREAK", "CONTINUE", 
            "FOR", "TO", "DOWNTO", "DO", "IF", "THEN", "ELSE", "RETURN", 
            "WHILE", "BEGIN", "END", "FUNCTION", "PROCEDURE", "VAR", "TRUE", 
            "FALSE", "ARRAY", "OF", "WITH", "REAL", "BOOLEAN", "INTEGER", 
            "STRING", "ADD", "SUB", "MUL", "DIV", "NOT", "MOD", "OR", "AND", 
            "NEQUAL", "EQUAL", "ASSIGN", "LETHANE", "GRETHANE", "LETHAN", 
            "GRETHAN", "INDIV", "LSB", "RSB", "COLON", "LB", "RB", "SEMI", 
            "DOUBLEDOT", "COMMA", "INTLIT", "FLOATLIT", "STRINGLIT", "ID", 
            "WS", "UNCLOSE_STRING", "ERROR_CHAR", "ILLEGAL_ESCAPE" ]

    ruleNames = [ "TRABLOCKCOMMENT", "BLOCKCOMMENT", "LINECOMMENT", "A", 
                  "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", 
                  "N", "O", "P", "R", "S", "T", "U", "V", "W", "Y", "BREAK", 
                  "CONTINUE", "FOR", "TO", "DOWNTO", "DO", "IF", "THEN", 
                  "ELSE", "RETURN", "WHILE", "BEGIN", "END", "FUNCTION", 
                  "PROCEDURE", "VAR", "TRUE", "FALSE", "ARRAY", "OF", "WITH", 
                  "REAL", "BOOLEAN", "INTEGER", "STRING", "ADD", "SUB", 
                  "MUL", "DIV", "NOT", "MOD", "OR", "AND", "NEQUAL", "EQUAL", 
                  "ASSIGN", "LETHANE", "GRETHANE", "LETHAN", "GRETHAN", 
                  "INDIV", "LSB", "RSB", "COLON", "LB", "RB", "SEMI", "DOUBLEDOT", 
                  "COMMA", "INTLIT", "NUMBER", "DEPT", "EXP", "DOT", "FLOATLIT", 
                  "ESCAPE", "STRINGLIT", "ID", "WS", "UNCLOSE_STRING", "ERROR_CHAR", 
                  "IES", "ILLEGAL_ESCAPE" ]

    grammarFileName = "MP.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def action(self, localctx:RuleContext, ruleIndex:int, actionIndex:int):
        if self._actions is None:
            actions = dict()
            actions[81] = self.STRINGLIT_action 
            actions[84] = self.UNCLOSE_STRING_action 
            actions[85] = self.ERROR_CHAR_action 
            actions[87] = self.ILLEGAL_ESCAPE_action 
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))

    def STRINGLIT_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 0:
            self.text =  self.text[1:len(self.text) - 1]
     

    def UNCLOSE_STRING_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 1:
             raise UncloseString(self.text[1:]) 
     

    def ERROR_CHAR_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 2:
            raise ErrorToken(self.text)
     

    def ILLEGAL_ESCAPE_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 3:
            raise IllegalEscape(self.text[1:])
     


