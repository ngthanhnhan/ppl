# Generated from main/mp/parser/MP.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .MPParser import MPParser
else:
    from MPParser import MPParser

# This class defines a complete generic visitor for a parse tree produced by MPParser.

class MPVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by MPParser#program.
    def visitProgram(self, ctx:MPParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#declaration.
    def visitDeclaration(self, ctx:MPParser.DeclarationContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#varDeclars.
    def visitVarDeclars(self, ctx:MPParser.VarDeclarsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#varDeclar.
    def visitVarDeclar(self, ctx:MPParser.VarDeclarContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#ids.
    def visitIds(self, ctx:MPParser.IdsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#typeVar.
    def visitTypeVar(self, ctx:MPParser.TypeVarContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#primitiveType.
    def visitPrimitiveType(self, ctx:MPParser.PrimitiveTypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#arrayType.
    def visitArrayType(self, ctx:MPParser.ArrayTypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#funcDeclar.
    def visitFuncDeclar(self, ctx:MPParser.FuncDeclarContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#paraList.
    def visitParaList(self, ctx:MPParser.ParaListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#paraDeclar.
    def visitParaDeclar(self, ctx:MPParser.ParaDeclarContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#compoundStmt.
    def visitCompoundStmt(self, ctx:MPParser.CompoundStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#procedureDeclar.
    def visitProcedureDeclar(self, ctx:MPParser.ProcedureDeclarContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expression.
    def visitExpression(self, ctx:MPParser.ExpressionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exp1.
    def visitExp1(self, ctx:MPParser.Exp1Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exp2.
    def visitExp2(self, ctx:MPParser.Exp2Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exp3.
    def visitExp3(self, ctx:MPParser.Exp3Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exp4.
    def visitExp4(self, ctx:MPParser.Exp4Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exp5.
    def visitExp5(self, ctx:MPParser.Exp5Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exp6.
    def visitExp6(self, ctx:MPParser.Exp6Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#invocationExp.
    def visitInvocationExp(self, ctx:MPParser.InvocationExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exp_list.
    def visitExp_list(self, ctx:MPParser.Exp_listContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#indexExpr.
    def visitIndexExpr(self, ctx:MPParser.IndexExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#assignSttm.
    def visitAssignSttm(self, ctx:MPParser.AssignSttmContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#lhs.
    def visitLhs(self, ctx:MPParser.LhsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#lhsItem.
    def visitLhsItem(self, ctx:MPParser.LhsItemContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#ifStatement.
    def visitIfStatement(self, ctx:MPParser.IfStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#whileStatement.
    def visitWhileStatement(self, ctx:MPParser.WhileStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#lstStatement.
    def visitLstStatement(self, ctx:MPParser.LstStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#forStatement.
    def visitForStatement(self, ctx:MPParser.ForStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#breakStatement.
    def visitBreakStatement(self, ctx:MPParser.BreakStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#continueStatement.
    def visitContinueStatement(self, ctx:MPParser.ContinueStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#returnStatement.
    def visitReturnStatement(self, ctx:MPParser.ReturnStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#withSttm.
    def visitWithSttm(self, ctx:MPParser.WithSttmContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#callFunc.
    def visitCallFunc(self, ctx:MPParser.CallFuncContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#statement.
    def visitStatement(self, ctx:MPParser.StatementContext):
        return self.visitChildren(ctx)



del MPParser