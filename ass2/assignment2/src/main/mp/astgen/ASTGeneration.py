from MPVisitor import MPVisitor
from MPParser import MPParser
from AST import *
from functools import reduce


class ASTGeneration(MPVisitor):
    # program: declaration+ EOF;
    def visitProgram(self, ctx: MPParser.ProgramContext):
        return Program([self.visit(x) for x in ctx.declaration()])

    # declaration: varDeclars | funcDeclar | procedureDeclar;
    def visitDeclaration(self, ctx: MPParser.DeclarationContext):
        if ctx.varDeclars() is not None:
            arr = ''
            for i in self.visit(ctx.varDeclars()):
                arr= arr+ ","+ (str(i))
            return arr[1:]
            #return self.visit(ctx.varDeclare())
        elif ctx.funcDeclar() is not None:
            return self.visit(ctx.funcDeclar())
        else :
            return self.visit(ctx.procedureDeclar())
            
    # varDeclars: VAR varDeclar+ ;
    def visitVarDeclars(self, ctx: MPParser.VarDeclarsContext):
        arr = []
        for item in ctx.varDeclar():
            for one in self.visit(item):
                arr.append(one)
        return arr 
    
    #   varDeclar: ids COLON typeVar SEMI;
    def visitVarDeclar(self, ctx:MPParser.VarDeclarContext):
        varType = self.visit(ctx.typeVar())
        return list(map(lambda x: VarDecl(x, varType), self.visit(ctx.ids())))

    # ids: ID COMMA ids | ID;
    def visitIds(self, ctx:MPParser.IdsContext):
        if ctx.COMMA():
            return [Id(ctx.ID().getText())] + self.visit(ctx.ids())
        else:
            return [Id(ctx.ID().getText())]

    # typeVar: primitiveType
	# | arrayType;
    def visitTypeVar(self, ctx: MPParser.TypeVarContext):
        return self.visit(ctx.getChild(0))

    # primitiveType:  INTEGER | BOOLEAN | REAL | STRING;
    def visitPrimitiveType(self, ctx: MPParser.PrimitiveTypeContext):
        if (ctx.INTEGER()):
            return IntType()
        elif (ctx.BOOLEAN()):
            return BoolType()
        elif (ctx.REAL()):
            return FloatType()
        else:
            return StringType()

    # arrayType: ARRAY LSB INTLIT DOUBLEDOT INTLIT RSB OF primitiveType;
    def visitArrayType(self, ctx: MPParser.ArrayTypeContext):
        return ArrayType(ctx.INTLIT(0), ctx.INTLIT(1), self.visit(ctx.primitiveType()))

    # funcDeclar:  FUNCTION ID LB paraList? RB COLON typeVar SEMI varDeclars? compoundStmt; 
    def visitFuncDeclar(self, ctx: MPParser.FuncDeclarContext):
        id = Id(ctx.ID().getText())
        paraList = self.visit(ctx.paraList()) if ctx.paraList() else []
        returnType = self.visit(ctx.typeVar())
        local = self.visit(ctx.varDeclars()) if ctx.varDeclars() else []
        arr = ''
        for i in self.visit(ctx.compoundStmt()):
            arr= arr+ ","+ (str(i))
        body = arr[1:]
        return FuncDecl(id, paraList, local, body, returnType)

    # paraList: paraDeclar (SEMI paraDeclar)*;
    def visitParaList(self, ctx: MPParser.ParaListContext):
        if ctx.SEMI() is not None:
            arr = []
            for item in ctx.paraDeclar():
                for one in self.visit(item):
                    arr.append(one)
            return arr 
        else :
            return [self.visit(ctx.paraDeclar())]

    def visitParaDeclar(self, ctx: MPParser.ParaDeclarContext):
        varType = self.visit(ctx.typeVar())
        return list(map(lambda x: VarDecl(x, varType), self.visit(ctx.ids())))

    # def visitLstId(self, ctx: MPParser.LstIdContext):
    #     return ''

    # def visitTypePara(self, ctx: MPParser.TypeParaContext):
    #     return ''

    # def visitReturnType(self, ctx: MPParser.ReturnTypeContext):
    #     return ''

    # compoundStmt: BEGIN statement* END;
    def visitCompoundStmt(self, ctx: MPParser.CompoundStmtContext):
        

    # procedureDeclar: PROCEDURE ID LB paraList? RB SEMI varDeclars? compoundStmt;
    def visitProcedureDeclar(self, ctx: MPParser.ProcedureDeclarContext):
        id = Id(ctx.ID().getText())
        paraList = self.visit(ctx.paraList()) if ctx.paraList() else []
        local = self.visit(ctx.varDeclars()) if ctx.varDeclars() else []
        body = self.visit(ctx.compoundStmt())
        return FuncDecl(id, paraList, local, body)

    def visitExpression(self, ctx: MPParser.ExpressionContext):
        return ''

    def visitExp1(self, ctx: MPParser.Exp1Context):
        return ''

    def visitExp2(self, ctx: MPParser.Exp2Context):
        return ''

    def visitExp3(self, ctx: MPParser.Exp3Context):
        return ''

    def visitExp4(self, ctx: MPParser.Exp4Context):
        return ''

    def visitExp5(self, ctx: MPParser.Exp5Context):
        return ''

    def visitExp6(self, ctx: MPParser.Exp6Context):
        return ''

    def visitInvocationExp(self, ctx: MPParser.InvocationExpContext):
        return ''

    def visitExp_list(self, ctx: MPParser.Exp_listContext):
        return ''

    def visitIndexExpr(self, ctx: MPParser.IndexExprContext):
        return 'index'

    # assignSttm: (lhs ASSIGN)+ expression SEMI; 
    def visitAssignSttm(self, ctx: MPParser.AssignSttmContext):
        exp = self.visit(ctx.expression())
        return list(map(lambda x: Assign(x, exp), self.visit(ctx.lhs())))

    # lhs: ID | indexExpr;
    def visitLhs(self, ctx: MPParser.LhsContext):
        if ctx.lhs():
            return [self.visit(ctx.lhsItem())] + [self.visit(ctx.lhs())]
        else:
             return [self.visit(ctx.lhsItem())]
    
    def visitLhsItem(self, ctx:MPParser.LhsItemContext):
        if ctx.ID():
            return Id(ctx.ID().getText())
        else:
            return self.visit(ctx.indexExpr())

    def visitIfStatement(self, ctx: MPParser.IfStatementContext):
        return ''

    def visitWhileStatement(self, ctx: MPParser.WhileStatementContext):
        return ''

    def visitLstStatement(self, ctx: MPParser.LstStatementContext):
        return ''

    def visitForStatement(self, ctx: MPParser.ForStatementContext):
        return ''

    def visitBreakStatement(self, ctx: MPParser.BreakStatementContext):
        return ''

    def visitContinueStatement(self, ctx: MPParser.ContinueStatementContext):
        return ''

    def visitReturnStatement(self, ctx: MPParser.ReturnStatementContext):
        return ''

    def visitWithSttm(self, ctx: MPParser.WithSttmContext):
        return ''

    def visitCallFunc(self, ctx: MPParser.CallFuncContext):
        return ''

    def visitStatement(self, ctx: MPParser.StatementContext):
        if ctx.assignSttm():
            return self.visit(ctx.assignSttm())

    # def visitProgram(self,ctx:MPParser.ProgramContext):
    #     return Program([self.visit(x) for x in ctx.decl()])
    #
    # def visitFuncdecl(self,ctx:MPParser.FuncdeclContext):
    #     local,cpstmt = self.visit(ctx.body())
    #     return FuncDecl(Id(ctx.ID().getText()),
    #                     [],
    #                     local,
    #                     cpstmt,
    #                     self.visit(ctx.mtype()))

    # def visitProcdecl(self,ctx:MPParser.ProcdeclContext):
    #     local,cpstmt = self.visit(ctx.body())
    #     return FuncDecl(Id(ctx.ID().getText()),
    #                     [],
    #                     local,
    #                     cpstmt)

    # def visitBody(self,ctx:MPParser.BodyContext):
    #     return [],[self.visit(x) for x in ctx.stmt()] if ctx.stmt() else []

    # def visitStmt(self,ctx:MPParser.StmtContext):
    #     return self.visit(ctx.funcall())

    # def visitFuncall(self,ctx:MPParser.FuncallContext):
    #     return CallStmt(Id(ctx.ID().getText()),[self.visit(ctx.exp())] if ctx.exp() else [])

    # def visitExp(self,ctx:MPParser.ExpContext):
    #     return IntLiteral(int(ctx.INTLIT().getText()))

    # def visitMtype(self,ctx:MPParser.MtypeContext):
    #     return IntType()
