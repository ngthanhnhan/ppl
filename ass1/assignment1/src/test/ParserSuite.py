import unittest
from TestUtils import TestParser

class ParserSuite(unittest.TestCase):
    def test_simple_program(self):
        """Simple program: int main() {} """
        input = """int main() {}"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,201))

    def test_more_complex_program(self):
        """More complex program"""
        input = """int main () {
            putIntLn(4);
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,202))
    
    def test_wrong_miss_close(self):
        """Miss ) int main( {}"""
        input = """int main( {}"""
        expect = "Error on line 1 col 10: {"
        self.assertTrue(TestParser.test(input,expect,203))

    def test_declare(self):
        """Miss ) int main( {}"""
        input = """function foo (a , b : integer ; c : real ) : array [1 .. 2] of integer ;
                    var x , y : real ;
                    begin
                        x := 1;
                        with a , b : integer ; c : array [ 1 .. 2 ] of real ; do
                            d := c [ a ] + b ;
                        foo (3 , a + 1, m( 2 ) ) ;
                    end
                    
                    procedure foo ( a , b : integer ; c : real) ;
                    var x , y : real ;
                    begin

                    end
                    procedure main ( ) ;
                    begin
                    end
                    
                    """
        expect = "Error on line 1 col 10: {"
        self.assertTrue(TestParser.test(input,expect,204))