import unittest
from TestUtils import TestLexer

class LexerSuite(unittest.TestCase):
      
    def test_identifier1(self):
        """test identifiers"""
        self.assertTrue(TestLexer.test("abc","abc,<EOF>",101))
    def test_identifier2(self):
        """test identifiers"""
        self.assertTrue(TestLexer.test("aCBbdc","aCBbdc,<EOF>",102))
    def test_identifier3(self):
        """test identifiers"""
        self.assertTrue(TestLexer.test("aAsVN","aAsVN,<EOF>",103))
    def test_integer(self):
        """test integers"""
        self.assertTrue(TestLexer.test("123a123","123,a123,<EOF>",104))
    def test_real(self):
        """test real"""
        self.assertTrue(TestLexer.test("1e3","1e3,<EOF>", 105))
    def test_keyword1(self):
        """test keyword"""
        self.assertTrue(TestLexer.test("boolean break","boolean,break,<EOF>", 106))
    def test_keyword2(self):
        """test keyword"""
        self.assertTrue(TestLexer.test("continue","continue,<EOF>", 107))
    def test_keyword3(self):
        """test keyword"""
        self.assertTrue(TestLexer.test("else for float","else,for,float,<EOF>", 108))
    def test_keyword4(self):
        """test keyword"""
        self.assertTrue(TestLexer.test("if int","if,int,<EOF>", 109))
    def test_keyword5(self):
        """test keyword"""
        self.assertTrue(TestLexer.test("return void","return,void,<EOF>", 110))
    def test_keyword6(self):
        """test keyword"""
        self.assertTrue(TestLexer.test("do","do,<EOF>", 111))
    def test_keyword7(self):
        """test keyword"""
        self.assertTrue(TestLexer.test("while","while,<EOF>", 112))
    def test_keyword8(self):
        """test keyword"""
        self.assertTrue(TestLexer.test("true","true,<EOF>", 113))
    def test_keyword9(self):
        """test keyword"""
        self.assertTrue(TestLexer.test("false","false,<EOF>", 114))
    def test_keyword10(self):
        """test keyword"""
        self.assertTrue(TestLexer.test("string","string,<EOF>", 115))
    def test_identifier4(self):
        """test identifiers"""
        self.assertTrue(TestLexer.test("_abc","_abc,<EOF>",116))
    def test_identifier5(self):
        """test identifiers"""
        self.assertTrue(TestLexer.test("string abc","string,abc,<EOF>",117))
    def test_identifier6(self):
        """test identifiers"""
        self.assertTrue(TestLexer.test("1abc","1,abc,<EOF>",118))
    def test_identifier7(self):
        """test identifiers"""
        self.assertTrue(TestLexer.test("abc123","abc123,<EOF>",119))
    def test_identifier8(self):
        """test identifiers"""
        self.assertTrue(TestLexer.test("ab123 12aa","ab123,12,aa,<EOF>",120))
    def test_operator1(self):
        """test operator"""
        self.assertTrue(TestLexer.test("x=y","x,=,y,<EOF>", 121))
    def test_operator2(self):
        """test operator"""
        self.assertTrue(TestLexer.test("a+b=3","a,+,b,=,3,<EOF>", 122))
    def test_operator3(self):
        """test operator"""
        self.assertTrue(TestLexer.test("y=g*5/2","y,=,g,*,5,/,2,<EOF>", 123))
    def test_operator4(self):
        """test operator"""
        self.assertTrue(TestLexer.test("m=n-5","m,=,n,-,5,<EOF>", 124))
    def test_operator5(self):
        """test operator"""
        self.assertTrue(TestLexer.test("-t or b","-,t,or,b,<EOF>", 125))
    def test_operator6(self):
        """test operator"""
        self.assertTrue(TestLexer.test("Not AnD","Not,AnD,<EOF>", 126))
    def test_operator7(self):
        """test operator"""
        self.assertTrue(TestLexer.test("x<>y==z","x,<>,y,=,=,z,<EOF>", 127))
    def test_operator8(self):
        """test operator"""
        self.assertTrue(TestLexer.test("x<y z<=t","x,<,y,z,<=,t,<EOF>", 128))
    def test_operator9(self):
        """test operator"""
        self.assertTrue(TestLexer.test("x>y","x,>,y,<EOF>", 129))
    def test_operator10(self):
        """test operator"""
        self.assertTrue(TestLexer.test("c>=>y","c,>=,>,y,<EOF>", 130))
    def test_separator1(self):
        """test separator"""
        self.assertTrue(TestLexer.test("begin int main end","begin,int,main,end,<EOF>", 131))
    def test_separator2(self):
        """test separator"""
        self.assertTrue(TestLexer.test("main int [","main,int,[,<EOF>", 132))
    def test_separator3(self):
        """test separator"""
        self.assertTrue(TestLexer.test("int b[3]","int,b,[,3,],<EOF>", 133))  
    def test_separator4(self):
        """test separator"""
        self.assertTrue(TestLexer.test("if (a)begin end","if,(,a,),begin,end,<EOF>", 134))
    def test_separator5(self):
        """test separator"""
        self.assertTrue(TestLexer.test(")(","),(,<EOF>", 135))
    def test_separator6(self):
        """test separator"""
        self.assertTrue(TestLexer.test("a,b","a,,,b,<EOF>", 136))
    def test_separator7(self):
        """test separator"""
        self.assertTrue(TestLexer.test("a,,b","a,,,,,b,<EOF>", 137))
    def test_separator8(self):
        """test separator"""
        self.assertTrue(TestLexer.test("foo();","foo,(,),;,<EOF>", 138))
    def test_separator9(self):
        """test separator"""
        self.assertTrue(TestLexer.test(";foo",";,foo,<EOF>", 139))
    def test_separator10(self):
        """test separator"""
        self.assertTrue(TestLexer.test(";foo;",";,foo,;,<EOF>", 140))
    def test_integer_literal1(self):
        """test integer literal"""
        self.assertTrue(TestLexer.test("123","123,<EOF>", 141))
    def test_integer_literal2(self):
        """test integer literal"""
        self.assertTrue(TestLexer.test("0123","0123,<EOF>", 142))
    def test_integer_literal3(self):
        """test integer literal"""
        self.assertTrue(TestLexer.test("12 14","12,14,<EOF>", 143))
    def test_float_literal1(self):
        """test float literal"""
        self.assertTrue(TestLexer.test("-1.2 1.3","-,1.2,1.3,<EOF>", 144))
    def test_float_literal2(self):
        """test float literal"""
        self.assertTrue(TestLexer.test("1. .1","1.,.1,<EOF>", 145))
    def test_float_literal3(self):
        """test float literal"""
        self.assertTrue(TestLexer.test(".","Error Token .", 146))
    def test_float_literal4(self):
        """test float literal"""
        self.assertTrue(TestLexer.test("1e2","1e2,<EOF>", 147))
    def test_float_literal5(self):
        """test float literal"""
        self.assertTrue(TestLexer.test("1.2E2","1.2E2,<EOF>", 148))
    def test_float_literal6(self):
        """test float literal"""
        self.assertTrue(TestLexer.test("1.2E2","1.2E2,<EOF>", 149))
    def test_float_literal7(self):
        """test float literal"""
        self.assertTrue(TestLexer.test("1.2E-2 1.2e+2","1.2E-2,1.2,e,+,2,<EOF>", 150))
    def test_float_literal8(self):
        """test float literal"""
        self.assertTrue(TestLexer.test(".1E2",".1E2,<EOF>", 151))
    def test_float_literal9(self):
        """test float literal"""
        self.assertTrue(TestLexer.test("128e-42","128e-42,<EOF>", 152))
    def test_float_literal10(self):
        """test float literal"""
        self.assertTrue(TestLexer.test("1.e2 .e","1.e2,Error Token .", 153))
    def test_float_literal11(self):
        """test float literal"""
        self.assertTrue(TestLexer.test("1.1e","1.1,e,<EOF>", 154))
    def test_float_literal12(self):
        """test float literal"""
        self.assertTrue(TestLexer.test("e-3","e,-,3,<EOF>", 155))
    def test_bool_literal1(self):
        """test float literal"""
        self.assertTrue(TestLexer.test("true","true,<EOF>", 156))
    def test_bool_literal2(self):
        """test float literal"""
        self.assertTrue(TestLexer.test("false","false,<EOF>", 157))
    def test_string_literal1(self):
        """test float literal"""
        self.assertTrue(TestLexer.test("""\"Hello\"""","Hello,<EOF>", 158))
    def test_line_comment1(self):
        """test line comment"""
        self.assertTrue(TestLexer.test("""//comment
        das
        sam""","das,sam,<EOF>", 159))
    def test_line_comment2(self):
        """test line comment"""
        self.assertTrue(TestLexer.test("""//comment 3243 %^&
        das
        sam""","das,sam,<EOF>", 160))
    def test_block_comment1(self):
        """test line comment"""
        self.assertTrue(TestLexer.test("""{* dasd*}""","<EOF>", 161))
    def test_block_comment2(self):
        """test block comment"""
        self.assertTrue(TestLexer.test("""{* dasd*} */""","*,/,<EOF>", 162))
    def test_block_comment3(self):
        """test block comment"""
        self.assertTrue(TestLexer.test("""(* d//asd\n
        ansdknsadksad
        asndksadnsa *)
        {* asbjd*}""","<EOF>", 163))
    def test_block_comment4(self):
        """test block comment"""
        self.assertTrue(TestLexer.test("""{* d//asd\n
        ansdknsadksad
        asndksadnsa *}
        {* asbjd*} *]""","*,],<EOF>", 164))
    def test_combination1(self):
        """test combination"""
        self.assertTrue(TestLexer.test("""int a=3;""","int,a,=,3,;,<EOF>", 165))
    def test_combination2(self):
        """test combination"""
        self.assertTrue(TestLexer.test("""int main () []""","int,main,(,),[,],<EOF>", 166))
    def test_combination3(self):
        """test combination"""
        self.assertTrue(TestLexer.test("""c/1.3e4""","c,/,1.3e4,<EOF>", 167))
    def test_combination4(self):
        """test combination"""
        self.assertTrue(TestLexer.test("""1,5E5 true2.2false""","1,,,5E5,true2,.2,false,<EOF>", 168))
    def test_combination5(self):
        """test combination"""
        self.assertTrue(TestLexer.test("""_cal+1ab _""","_cal,+,1,ab,_,<EOF>", 169))
    def test_combination6(self):
        """test combination"""
        self.assertTrue(TestLexer.test(""".1E-5+5 true2.false""",".1E-5,+,5,true2,Error Token .", 170))
    def test_combination7(self):
        """test combination"""
        self.assertTrue(TestLexer.test("""x+{*ert// *}""","x,+,<EOF>", 171))
    def test_combination8(self):
        """test combination"""
        self.assertTrue(TestLexer.test("""1.4abc""","1.4,abc,<EOF>", 172))
    def test_illegal_escape1(self):
        """test illegal escape"""
        self.assertTrue(TestLexer.test(""" \"dasd \' \ a\" ""","Illegal Escape In String: dasd ' \ ", 173))
    def test_unclose_string1(self):
        """test illegal escape"""
        self.assertTrue(TestLexer.test(""" \"dasd dasd ""","Unclosed String: dasd dasd ", 174))
    def test_illegal_escape2(self):
        """test illegal escape"""
        self.assertTrue(TestLexer.test(""" \"dasd dasd \\ \\" \" ""","Illegal Escape In String: dasd dasd \ ", 175))
    def test_unclose_string2(self):
        """test illegal escape"""
        self.assertTrue(TestLexer.test(""" \"dasd\n dasd\" ""","""Unclosed String: dasd
""", 176))
    def test_illegal_escape3(self):
        """test unclose string"""
        self.assertTrue(TestLexer.test(""" \"dasd \\ \"
        dasd\" ""","""Illegal Escape In String: dasd \ """, 177))
    def test_unclose_string3(self):
        """test unclose string"""
        self.assertTrue(TestLexer.test(""" \"""","""Unclosed String: """, 178))
    def test_error_token1(self):
        """test error token"""
        self.assertTrue(TestLexer.test(""" .""","""Error Token .""", 179))
    def test_error_token2(self):
        """test error token"""
        self.assertTrue(TestLexer.test(""" &""","""Error Token &""", 180))
    def test_error_token3(self):
        """test error token"""
        self.assertTrue(TestLexer.test(""" ~ aaa""","""Error Token ~""", 181))
    def test_error_token4(self):
        """test error token"""
        self.assertTrue(TestLexer.test(""" abc @""","""abc,Error Token @""", 182))
    def test_error_token5(self):
        """test error token"""
        self.assertTrue(TestLexer.test(""" abc #""","""abc,Error Token #""", 183))
    def test_combination9(self):
        """test combination"""
        self.assertTrue(TestLexer.test("""string s=\"hello\"""","string,s,=,hello,<EOF>", 184))
    def test_combination10(self):
        """test combination"""
        self.assertTrue(TestLexer.test("""float t = .""","float,t,=,Error Token .", 185))
    def test_combination11(self):
        """test combination"""
        self.assertTrue(TestLexer.test("""x=\"this \n \"""","""x,=,Unclosed String: this 
        """, 186))
    def test_combination12(self):
        """test combination"""
        self.assertTrue(TestLexer.test("""\" \' ""","""Unclosed String:  ' """, 187))
    def test_combination13(self):
        """test combination"""
        self.assertTrue(TestLexer.test(""" \"\\n\"\"\' ""","""\\n,Unclosed String: ' """, 188))
    def test_combination14(self):
        """test combination"""
        self.assertTrue(TestLexer.test(""" \"EOF ""","""Unclosed String: EOF """, 189))
    def test_combination15(self):
        """test combination"""
        self.assertTrue(TestLexer.test(""" <EOF> ""","""<,EOF,>,<EOF>""", 190))
    