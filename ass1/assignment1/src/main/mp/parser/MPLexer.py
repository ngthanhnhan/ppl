# Generated from main/mp/parser/MP.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


from lexererr import *;


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2<")
        buf.write("\u01d7\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:")
        buf.write("\4;\t;\4<\t<\4=\t=\4>\t>\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5")
        buf.write("\3\5\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3")
        buf.write("\b\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13")
        buf.write("\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r")
        buf.write("\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17")
        buf.write("\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21")
        buf.write("\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22")
        buf.write("\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\25")
        buf.write("\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\27\3\27")
        buf.write("\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\30")
        buf.write("\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\32")
        buf.write("\3\32\7\32\u0108\n\32\f\32\16\32\u010b\13\32\3\33\3\33")
        buf.write("\3\34\3\34\3\35\3\35\3\36\3\36\3\37\3\37\3 \3 \3!\3!\3")
        buf.write("!\3\"\3\"\3#\6#\u011f\n#\r#\16#\u0120\3$\7$\u0124\n$\f")
        buf.write("$\16$\u0127\13$\3$\3$\6$\u012b\n$\r$\16$\u012c\3$\6$\u0130")
        buf.write("\n$\r$\16$\u0131\3$\3$\7$\u0136\n$\f$\16$\u0139\13$\5")
        buf.write("$\u013b\n$\3%\3%\6%\u013f\n%\r%\16%\u0140\5%\u0143\n%")
        buf.write("\3%\3%\5%\u0147\n%\3%\6%\u014a\n%\r%\16%\u014b\3&\3&\5")
        buf.write("&\u0150\n&\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\5\'\u015b")
        buf.write("\n\'\3(\3(\3(\7(\u0160\n(\f(\16(\u0163\13(\3(\3(\3(\3")
        buf.write(")\3)\3)\3*\3*\3+\3+\3,\3,\3-\3-\3.\3.\3.\3.\3/\3/\3/\3")
        buf.write("/\3\60\3\60\3\60\3\61\3\61\3\61\3\61\3\62\3\62\3\62\3")
        buf.write("\63\3\63\3\64\3\64\3\65\3\65\3\66\3\66\3\66\3\67\3\67")
        buf.write("\3\67\38\38\38\38\39\39\39\39\79\u0199\n9\f9\169\u019c")
        buf.write("\139\39\39\3:\3:\3:\3:\7:\u01a4\n:\f:\16:\u01a7\13:\3")
        buf.write(":\3:\3:\3:\7:\u01ad\n:\f:\16:\u01b0\13:\3:\3:\5:\u01b4")
        buf.write("\n:\3:\3:\3;\6;\u01b9\n;\r;\16;\u01ba\3;\3;\3<\3<\3<\7")
        buf.write("<\u01c2\n<\f<\16<\u01c5\13<\3<\3<\3<\3<\3=\3=\3=\7=\u01ce")
        buf.write("\n=\f=\16=\u01d1\13=\3=\3=\3>\3>\3>\5\u01a5\u01ae\u01c3")
        buf.write("\2?\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r")
        buf.write("\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30")
        buf.write("/\31\61\32\63\33\65\34\67\359\36;\37= ?!A\"C#E$G\2I\2")
        buf.write("K%M&O\'Q\2S(U)W*Y+[,]-_.a/c\60e\61g\62i\63k\64m\65o\66")
        buf.write("q\67s8u9w:y;{<\3\2 \4\2DDdd\4\2TTtt\4\2GGgg\4\2CCcc\4")
        buf.write("\2MMmm\4\2EEee\4\2QQqq\4\2PPpp\4\2VVvv\4\2KKkk\4\2WWw")
        buf.write("w\4\2HHhh\4\2FFff\4\2YYyy\4\2JJjj\4\2NNnn\4\2UUuu\4\2")
        buf.write("IIii\4\2RRrr\4\2XXxx\4\2[[{{\5\2C\\aac|\6\2\62;C\\aac")
        buf.write("|\3\2\62;\3\2//\5\2\f\f$$^^\n\2$$))^^ddhhppttvv\4\2OO")
        buf.write("oo\3\2\f\f\5\2\13\f\17\17\"\"\2\u01eb\2\3\3\2\2\2\2\5")
        buf.write("\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2")
        buf.write("\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2")
        buf.write("\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2")
        buf.write("\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2")
        buf.write("\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61")
        buf.write("\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2")
        buf.write("\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3")
        buf.write("\2\2\2\2E\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2S")
        buf.write("\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2")
        buf.write("]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2")
        buf.write("\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2o\3\2\2")
        buf.write("\2\2q\3\2\2\2\2s\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2\2y\3\2")
        buf.write("\2\2\2{\3\2\2\2\3}\3\2\2\2\5\u0080\3\2\2\2\7\u0086\3\2")
        buf.write("\2\2\t\u008f\3\2\2\2\13\u0093\3\2\2\2\r\u0096\3\2\2\2")
        buf.write("\17\u009d\3\2\2\2\21\u00a0\3\2\2\2\23\u00a3\3\2\2\2\25")
        buf.write("\u00a8\3\2\2\2\27\u00ad\3\2\2\2\31\u00b4\3\2\2\2\33\u00ba")
        buf.write("\3\2\2\2\35\u00c0\3\2\2\2\37\u00c4\3\2\2\2!\u00cd\3\2")
        buf.write("\2\2#\u00d7\3\2\2\2%\u00db\3\2\2\2\'\u00e1\3\2\2\2)\u00e4")
        buf.write("\3\2\2\2+\u00e9\3\2\2\2-\u00ee\3\2\2\2/\u00f6\3\2\2\2")
        buf.write("\61\u00fe\3\2\2\2\63\u0105\3\2\2\2\65\u010c\3\2\2\2\67")
        buf.write("\u010e\3\2\2\29\u0110\3\2\2\2;\u0112\3\2\2\2=\u0114\3")
        buf.write("\2\2\2?\u0116\3\2\2\2A\u0118\3\2\2\2C\u011b\3\2\2\2E\u011e")
        buf.write("\3\2\2\2G\u013a\3\2\2\2I\u0142\3\2\2\2K\u014f\3\2\2\2")
        buf.write("M\u015a\3\2\2\2O\u015c\3\2\2\2Q\u0167\3\2\2\2S\u016a\3")
        buf.write("\2\2\2U\u016c\3\2\2\2W\u016e\3\2\2\2Y\u0170\3\2\2\2[\u0172")
        buf.write("\3\2\2\2]\u0176\3\2\2\2_\u017a\3\2\2\2a\u017d\3\2\2\2")
        buf.write("c\u0181\3\2\2\2e\u0184\3\2\2\2g\u0186\3\2\2\2i\u0188\3")
        buf.write("\2\2\2k\u018a\3\2\2\2m\u018d\3\2\2\2o\u0190\3\2\2\2q\u0194")
        buf.write("\3\2\2\2s\u01b3\3\2\2\2u\u01b8\3\2\2\2w\u01be\3\2\2\2")
        buf.write("y\u01ca\3\2\2\2{\u01d4\3\2\2\2}~\7<\2\2~\177\7?\2\2\177")
        buf.write("\4\3\2\2\2\u0080\u0081\t\2\2\2\u0081\u0082\t\3\2\2\u0082")
        buf.write("\u0083\t\4\2\2\u0083\u0084\t\5\2\2\u0084\u0085\t\6\2\2")
        buf.write("\u0085\6\3\2\2\2\u0086\u0087\t\7\2\2\u0087\u0088\t\b\2")
        buf.write("\2\u0088\u0089\t\t\2\2\u0089\u008a\t\n\2\2\u008a\u008b")
        buf.write("\t\13\2\2\u008b\u008c\t\t\2\2\u008c\u008d\t\f\2\2\u008d")
        buf.write("\u008e\t\4\2\2\u008e\b\3\2\2\2\u008f\u0090\t\r\2\2\u0090")
        buf.write("\u0091\t\b\2\2\u0091\u0092\t\3\2\2\u0092\n\3\2\2\2\u0093")
        buf.write("\u0094\t\n\2\2\u0094\u0095\t\b\2\2\u0095\f\3\2\2\2\u0096")
        buf.write("\u0097\t\16\2\2\u0097\u0098\t\b\2\2\u0098\u0099\t\17\2")
        buf.write("\2\u0099\u009a\t\t\2\2\u009a\u009b\t\n\2\2\u009b\u009c")
        buf.write("\t\b\2\2\u009c\16\3\2\2\2\u009d\u009e\t\16\2\2\u009e\u009f")
        buf.write("\t\b\2\2\u009f\20\3\2\2\2\u00a0\u00a1\t\13\2\2\u00a1\u00a2")
        buf.write("\t\r\2\2\u00a2\22\3\2\2\2\u00a3\u00a4\t\n\2\2\u00a4\u00a5")
        buf.write("\t\20\2\2\u00a5\u00a6\t\4\2\2\u00a6\u00a7\t\t\2\2\u00a7")
        buf.write("\24\3\2\2\2\u00a8\u00a9\t\4\2\2\u00a9\u00aa\t\21\2\2\u00aa")
        buf.write("\u00ab\t\22\2\2\u00ab\u00ac\t\4\2\2\u00ac\26\3\2\2\2\u00ad")
        buf.write("\u00ae\t\3\2\2\u00ae\u00af\t\4\2\2\u00af\u00b0\t\n\2\2")
        buf.write("\u00b0\u00b1\t\f\2\2\u00b1\u00b2\t\3\2\2\u00b2\u00b3\t")
        buf.write("\t\2\2\u00b3\30\3\2\2\2\u00b4\u00b5\t\17\2\2\u00b5\u00b6")
        buf.write("\t\20\2\2\u00b6\u00b7\t\13\2\2\u00b7\u00b8\t\21\2\2\u00b8")
        buf.write("\u00b9\t\4\2\2\u00b9\32\3\2\2\2\u00ba\u00bb\t\2\2\2\u00bb")
        buf.write("\u00bc\t\4\2\2\u00bc\u00bd\t\23\2\2\u00bd\u00be\t\13\2")
        buf.write("\2\u00be\u00bf\t\t\2\2\u00bf\34\3\2\2\2\u00c0\u00c1\t")
        buf.write("\4\2\2\u00c1\u00c2\t\t\2\2\u00c2\u00c3\t\16\2\2\u00c3")
        buf.write("\36\3\2\2\2\u00c4\u00c5\t\r\2\2\u00c5\u00c6\t\f\2\2\u00c6")
        buf.write("\u00c7\t\t\2\2\u00c7\u00c8\t\7\2\2\u00c8\u00c9\t\n\2\2")
        buf.write("\u00c9\u00ca\t\13\2\2\u00ca\u00cb\t\b\2\2\u00cb\u00cc")
        buf.write("\t\t\2\2\u00cc \3\2\2\2\u00cd\u00ce\t\24\2\2\u00ce\u00cf")
        buf.write("\t\3\2\2\u00cf\u00d0\t\b\2\2\u00d0\u00d1\t\7\2\2\u00d1")
        buf.write("\u00d2\t\4\2\2\u00d2\u00d3\t\16\2\2\u00d3\u00d4\t\f\2")
        buf.write("\2\u00d4\u00d5\t\3\2\2\u00d5\u00d6\t\4\2\2\u00d6\"\3\2")
        buf.write("\2\2\u00d7\u00d8\t\25\2\2\u00d8\u00d9\t\5\2\2\u00d9\u00da")
        buf.write("\t\3\2\2\u00da$\3\2\2\2\u00db\u00dc\t\5\2\2\u00dc\u00dd")
        buf.write("\t\3\2\2\u00dd\u00de\t\3\2\2\u00de\u00df\t\5\2\2\u00df")
        buf.write("\u00e0\t\26\2\2\u00e0&\3\2\2\2\u00e1\u00e2\t\b\2\2\u00e2")
        buf.write("\u00e3\t\r\2\2\u00e3(\3\2\2\2\u00e4\u00e5\t\17\2\2\u00e5")
        buf.write("\u00e6\t\13\2\2\u00e6\u00e7\t\n\2\2\u00e7\u00e8\t\20\2")
        buf.write("\2\u00e8*\3\2\2\2\u00e9\u00ea\t\3\2\2\u00ea\u00eb\t\4")
        buf.write("\2\2\u00eb\u00ec\t\5\2\2\u00ec\u00ed\t\21\2\2\u00ed,\3")
        buf.write("\2\2\2\u00ee\u00ef\t\2\2\2\u00ef\u00f0\t\b\2\2\u00f0\u00f1")
        buf.write("\t\b\2\2\u00f1\u00f2\t\21\2\2\u00f2\u00f3\t\4\2\2\u00f3")
        buf.write("\u00f4\t\5\2\2\u00f4\u00f5\t\t\2\2\u00f5.\3\2\2\2\u00f6")
        buf.write("\u00f7\t\13\2\2\u00f7\u00f8\t\t\2\2\u00f8\u00f9\t\n\2")
        buf.write("\2\u00f9\u00fa\t\4\2\2\u00fa\u00fb\t\23\2\2\u00fb\u00fc")
        buf.write("\t\4\2\2\u00fc\u00fd\t\3\2\2\u00fd\60\3\2\2\2\u00fe\u00ff")
        buf.write("\t\22\2\2\u00ff\u0100\t\n\2\2\u0100\u0101\t\3\2\2\u0101")
        buf.write("\u0102\t\13\2\2\u0102\u0103\t\t\2\2\u0103\u0104\t\23\2")
        buf.write("\2\u0104\62\3\2\2\2\u0105\u0109\t\27\2\2\u0106\u0108\t")
        buf.write("\30\2\2\u0107\u0106\3\2\2\2\u0108\u010b\3\2\2\2\u0109")
        buf.write("\u0107\3\2\2\2\u0109\u010a\3\2\2\2\u010a\64\3\2\2\2\u010b")
        buf.write("\u0109\3\2\2\2\u010c\u010d\7]\2\2\u010d\66\3\2\2\2\u010e")
        buf.write("\u010f\7_\2\2\u010f8\3\2\2\2\u0110\u0111\7<\2\2\u0111")
        buf.write(":\3\2\2\2\u0112\u0113\7*\2\2\u0113<\3\2\2\2\u0114\u0115")
        buf.write("\7+\2\2\u0115>\3\2\2\2\u0116\u0117\7=\2\2\u0117@\3\2\2")
        buf.write("\2\u0118\u0119\7\60\2\2\u0119\u011a\7\60\2\2\u011aB\3")
        buf.write("\2\2\2\u011b\u011c\7.\2\2\u011cD\3\2\2\2\u011d\u011f\t")
        buf.write("\31\2\2\u011e\u011d\3\2\2\2\u011f\u0120\3\2\2\2\u0120")
        buf.write("\u011e\3\2\2\2\u0120\u0121\3\2\2\2\u0121F\3\2\2\2\u0122")
        buf.write("\u0124\t\31\2\2\u0123\u0122\3\2\2\2\u0124\u0127\3\2\2")
        buf.write("\2\u0125\u0123\3\2\2\2\u0125\u0126\3\2\2\2\u0126\u0128")
        buf.write("\3\2\2\2\u0127\u0125\3\2\2\2\u0128\u012a\7\60\2\2\u0129")
        buf.write("\u012b\t\31\2\2\u012a\u0129\3\2\2\2\u012b\u012c\3\2\2")
        buf.write("\2\u012c\u012a\3\2\2\2\u012c\u012d\3\2\2\2\u012d\u013b")
        buf.write("\3\2\2\2\u012e\u0130\t\31\2\2\u012f\u012e\3\2\2\2\u0130")
        buf.write("\u0131\3\2\2\2\u0131\u012f\3\2\2\2\u0131\u0132\3\2\2\2")
        buf.write("\u0132\u0133\3\2\2\2\u0133\u0137\7\60\2\2\u0134\u0136")
        buf.write("\t\31\2\2\u0135\u0134\3\2\2\2\u0136\u0139\3\2\2\2\u0137")
        buf.write("\u0135\3\2\2\2\u0137\u0138\3\2\2\2\u0138\u013b\3\2\2\2")
        buf.write("\u0139\u0137\3\2\2\2\u013a\u0125\3\2\2\2\u013a\u012f\3")
        buf.write("\2\2\2\u013bH\3\2\2\2\u013c\u0143\5G$\2\u013d\u013f\t")
        buf.write("\31\2\2\u013e\u013d\3\2\2\2\u013f\u0140\3\2\2\2\u0140")
        buf.write("\u013e\3\2\2\2\u0140\u0141\3\2\2\2\u0141\u0143\3\2\2\2")
        buf.write("\u0142\u013c\3\2\2\2\u0142\u013e\3\2\2\2\u0143\u0144\3")
        buf.write("\2\2\2\u0144\u0146\t\4\2\2\u0145\u0147\t\32\2\2\u0146")
        buf.write("\u0145\3\2\2\2\u0146\u0147\3\2\2\2\u0147\u0149\3\2\2\2")
        buf.write("\u0148\u014a\t\31\2\2\u0149\u0148\3\2\2\2\u014a\u014b")
        buf.write("\3\2\2\2\u014b\u0149\3\2\2\2\u014b\u014c\3\2\2\2\u014c")
        buf.write("J\3\2\2\2\u014d\u0150\5G$\2\u014e\u0150\5I%\2\u014f\u014d")
        buf.write("\3\2\2\2\u014f\u014e\3\2\2\2\u0150L\3\2\2\2\u0151\u0152")
        buf.write("\t\n\2\2\u0152\u0153\t\3\2\2\u0153\u0154\t\f\2\2\u0154")
        buf.write("\u015b\t\4\2\2\u0155\u0156\t\r\2\2\u0156\u0157\t\5\2\2")
        buf.write("\u0157\u0158\t\21\2\2\u0158\u0159\t\22\2\2\u0159\u015b")
        buf.write("\t\4\2\2\u015a\u0151\3\2\2\2\u015a\u0155\3\2\2\2\u015b")
        buf.write("N\3\2\2\2\u015c\u0161\7$\2\2\u015d\u0160\n\33\2\2\u015e")
        buf.write("\u0160\5Q)\2\u015f\u015d\3\2\2\2\u015f\u015e\3\2\2\2\u0160")
        buf.write("\u0163\3\2\2\2\u0161\u015f\3\2\2\2\u0161\u0162\3\2\2\2")
        buf.write("\u0162\u0164\3\2\2\2\u0163\u0161\3\2\2\2\u0164\u0165\7")
        buf.write("$\2\2\u0165\u0166\b(\2\2\u0166P\3\2\2\2\u0167\u0168\7")
        buf.write("^\2\2\u0168\u0169\t\34\2\2\u0169R\3\2\2\2\u016a\u016b")
        buf.write("\7-\2\2\u016bT\3\2\2\2\u016c\u016d\7/\2\2\u016dV\3\2\2")
        buf.write("\2\u016e\u016f\7,\2\2\u016fX\3\2\2\2\u0170\u0171\7\61")
        buf.write("\2\2\u0171Z\3\2\2\2\u0172\u0173\t\t\2\2\u0173\u0174\t")
        buf.write("\b\2\2\u0174\u0175\t\n\2\2\u0175\\\3\2\2\2\u0176\u0177")
        buf.write("\t\35\2\2\u0177\u0178\t\b\2\2\u0178\u0179\t\16\2\2\u0179")
        buf.write("^\3\2\2\2\u017a\u017b\t\b\2\2\u017b\u017c\t\3\2\2\u017c")
        buf.write("`\3\2\2\2\u017d\u017e\t\5\2\2\u017e\u017f\t\t\2\2\u017f")
        buf.write("\u0180\t\16\2\2\u0180b\3\2\2\2\u0181\u0182\7>\2\2\u0182")
        buf.write("\u0183\7@\2\2\u0183d\3\2\2\2\u0184\u0185\7?\2\2\u0185")
        buf.write("f\3\2\2\2\u0186\u0187\7>\2\2\u0187h\3\2\2\2\u0188\u0189")
        buf.write("\7@\2\2\u0189j\3\2\2\2\u018a\u018b\7>\2\2\u018b\u018c")
        buf.write("\7?\2\2\u018cl\3\2\2\2\u018d\u018e\7@\2\2\u018e\u018f")
        buf.write("\7?\2\2\u018fn\3\2\2\2\u0190\u0191\t\16\2\2\u0191\u0192")
        buf.write("\t\13\2\2\u0192\u0193\t\25\2\2\u0193p\3\2\2\2\u0194\u0195")
        buf.write("\7\61\2\2\u0195\u0196\7\61\2\2\u0196\u019a\3\2\2\2\u0197")
        buf.write("\u0199\n\36\2\2\u0198\u0197\3\2\2\2\u0199\u019c\3\2\2")
        buf.write("\2\u019a\u0198\3\2\2\2\u019a\u019b\3\2\2\2\u019b\u019d")
        buf.write("\3\2\2\2\u019c\u019a\3\2\2\2\u019d\u019e\b9\3\2\u019e")
        buf.write("r\3\2\2\2\u019f\u01a0\7*\2\2\u01a0\u01a1\7,\2\2\u01a1")
        buf.write("\u01a5\3\2\2\2\u01a2\u01a4\13\2\2\2\u01a3\u01a2\3\2\2")
        buf.write("\2\u01a4\u01a7\3\2\2\2\u01a5\u01a6\3\2\2\2\u01a5\u01a3")
        buf.write("\3\2\2\2\u01a6\u01a8\3\2\2\2\u01a7\u01a5\3\2\2\2\u01a8")
        buf.write("\u01a9\7,\2\2\u01a9\u01b4\7+\2\2\u01aa\u01ae\7}\2\2\u01ab")
        buf.write("\u01ad\13\2\2\2\u01ac\u01ab\3\2\2\2\u01ad\u01b0\3\2\2")
        buf.write("\2\u01ae\u01af\3\2\2\2\u01ae\u01ac\3\2\2\2\u01af\u01b1")
        buf.write("\3\2\2\2\u01b0\u01ae\3\2\2\2\u01b1\u01b2\7,\2\2\u01b2")
        buf.write("\u01b4\7\177\2\2\u01b3\u019f\3\2\2\2\u01b3\u01aa\3\2\2")
        buf.write("\2\u01b4\u01b5\3\2\2\2\u01b5\u01b6\b:\3\2\u01b6t\3\2\2")
        buf.write("\2\u01b7\u01b9\t\37\2\2\u01b8\u01b7\3\2\2\2\u01b9\u01ba")
        buf.write("\3\2\2\2\u01ba\u01b8\3\2\2\2\u01ba\u01bb\3\2\2\2\u01bb")
        buf.write("\u01bc\3\2\2\2\u01bc\u01bd\b;\3\2\u01bdv\3\2\2\2\u01be")
        buf.write("\u01c3\7$\2\2\u01bf\u01c2\n\33\2\2\u01c0\u01c2\5Q)\2\u01c1")
        buf.write("\u01bf\3\2\2\2\u01c1\u01c0\3\2\2\2\u01c2\u01c5\3\2\2\2")
        buf.write("\u01c3\u01c4\3\2\2\2\u01c3\u01c1\3\2\2\2\u01c4\u01c6\3")
        buf.write("\2\2\2\u01c5\u01c3\3\2\2\2\u01c6\u01c7\7^\2\2\u01c7\u01c8")
        buf.write("\n\34\2\2\u01c8\u01c9\b<\4\2\u01c9x\3\2\2\2\u01ca\u01cf")
        buf.write("\7$\2\2\u01cb\u01ce\n\33\2\2\u01cc\u01ce\5Q)\2\u01cd\u01cb")
        buf.write("\3\2\2\2\u01cd\u01cc\3\2\2\2\u01ce\u01d1\3\2\2\2\u01cf")
        buf.write("\u01cd\3\2\2\2\u01cf\u01d0\3\2\2\2\u01d0\u01d2\3\2\2\2")
        buf.write("\u01d1\u01cf\3\2\2\2\u01d2\u01d3\b=\5\2\u01d3z\3\2\2\2")
        buf.write("\u01d4\u01d5\13\2\2\2\u01d5\u01d6\b>\6\2\u01d6|\3\2\2")
        buf.write("\2\33\2\u0109\u0120\u0125\u012c\u0131\u0137\u013a\u0140")
        buf.write("\u0142\u0146\u014b\u014f\u015a\u015f\u0161\u019a\u01a5")
        buf.write("\u01ae\u01b3\u01ba\u01c1\u01c3\u01cd\u01cf\7\3(\2\b\2")
        buf.write("\2\3<\3\3=\4\3>\5")
        return buf.getvalue()


class MPLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    BREAK = 2
    CONTINUE = 3
    FOR = 4
    TO = 5
    DOWNTO = 6
    DO = 7
    IF = 8
    THEN = 9
    ELSE = 10
    RETURN = 11
    WHILE = 12
    BEGIN = 13
    END = 14
    FUNCTION = 15
    PROCEDURE = 16
    VAR = 17
    ARRAY = 18
    OF = 19
    WITH = 20
    REALmptype = 21
    BOOLmptype = 22
    INTmptype = 23
    STRINGmptype = 24
    ID = 25
    LSB = 26
    RSB = 27
    COLON = 28
    LB = 29
    RB = 30
    SEMI = 31
    DOUDOT = 32
    CM = 33
    INTLIT = 34
    FLOATLIT = 35
    BOOLLIT = 36
    STRINGLIT = 37
    ADD = 38
    SUB = 39
    MUL = 40
    DIV = 41
    NOT = 42
    MOD = 43
    OR = 44
    AND = 45
    NOTEQUAL = 46
    EQUAL = 47
    LESSTHAN = 48
    GREATERTHAN = 49
    LESSTHANOREQUAL = 50
    GREATERTHANOREQUAL = 51
    INTDIV = 52
    LINECOMMENT = 53
    BLOCKCOMMENT = 54
    WS = 55
    ILLEGAL_ESCAPE = 56
    UNCLOSE_STRING = 57
    ERROR_CHAR = 58

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "':='", "'['", "']'", "':'", "'('", "')'", "';'", "'..'", "','", 
            "'+'", "'-'", "'*'", "'/'", "'<>'", "'='", "'<'", "'>'", "'<='", 
            "'>='" ]

    symbolicNames = [ "<INVALID>",
            "BREAK", "CONTINUE", "FOR", "TO", "DOWNTO", "DO", "IF", "THEN", 
            "ELSE", "RETURN", "WHILE", "BEGIN", "END", "FUNCTION", "PROCEDURE", 
            "VAR", "ARRAY", "OF", "WITH", "REALmptype", "BOOLmptype", "INTmptype", 
            "STRINGmptype", "ID", "LSB", "RSB", "COLON", "LB", "RB", "SEMI", 
            "DOUDOT", "CM", "INTLIT", "FLOATLIT", "BOOLLIT", "STRINGLIT", 
            "ADD", "SUB", "MUL", "DIV", "NOT", "MOD", "OR", "AND", "NOTEQUAL", 
            "EQUAL", "LESSTHAN", "GREATERTHAN", "LESSTHANOREQUAL", "GREATERTHANOREQUAL", 
            "INTDIV", "LINECOMMENT", "BLOCKCOMMENT", "WS", "ILLEGAL_ESCAPE", 
            "UNCLOSE_STRING", "ERROR_CHAR" ]

    ruleNames = [ "T__0", "BREAK", "CONTINUE", "FOR", "TO", "DOWNTO", "DO", 
                  "IF", "THEN", "ELSE", "RETURN", "WHILE", "BEGIN", "END", 
                  "FUNCTION", "PROCEDURE", "VAR", "ARRAY", "OF", "WITH", 
                  "REALmptype", "BOOLmptype", "INTmptype", "STRINGmptype", 
                  "ID", "LSB", "RSB", "COLON", "LB", "RB", "SEMI", "DOUDOT", 
                  "CM", "INTLIT", "DECIMAL_FLOAT", "EXPONENT_FLOAT", "FLOATLIT", 
                  "BOOLLIT", "STRINGLIT", "ESCAPESTRING", "ADD", "SUB", 
                  "MUL", "DIV", "NOT", "MOD", "OR", "AND", "NOTEQUAL", "EQUAL", 
                  "LESSTHAN", "GREATERTHAN", "LESSTHANOREQUAL", "GREATERTHANOREQUAL", 
                  "INTDIV", "LINECOMMENT", "BLOCKCOMMENT", "WS", "ILLEGAL_ESCAPE", 
                  "UNCLOSE_STRING", "ERROR_CHAR" ]

    grammarFileName = "MP.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def action(self, localctx:RuleContext, ruleIndex:int, actionIndex:int):
        if self._actions is None:
            actions = dict()
            actions[38] = self.STRINGLIT_action 
            actions[58] = self.ILLEGAL_ESCAPE_action 
            actions[59] = self.UNCLOSE_STRING_action 
            actions[60] = self.ERROR_CHAR_action 
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))

    def STRINGLIT_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 0:
            self.text = self.text[1:len(self.text)-1]
     

    def ILLEGAL_ESCAPE_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 1:
            raise IllegalEscape(self.text[1:])
     

    def UNCLOSE_STRING_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 2:
            raise UncloseString(self.text[1:])
     

    def ERROR_CHAR_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 3:
            raise ErrorToken(self.text)
     


