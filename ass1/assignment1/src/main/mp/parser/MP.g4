/**
* Student name: Nguyen Thanh Nhan
* Student ID: 1512266
*/

grammar MP;

@lexer::header {
from lexererr import *;
}

options{
	language=Python3;
}

// Program structure
program  : declaration+ EOF;

declaration: variableDeclaration | functionDeclaration | procedureDeclaration;

variableDeclaration: VAR variableList COLON mptype SEMI;

variableList: ID (CM ID)*;

functionDeclaration: FUNCTION ID LB paramList? RB COLON mptype SEMI variableDeclaration? compoundStmt;

procedureDeclaration: PROCEDURE ID LB paramList? RB SEMI variableDeclaration compoundStmt;

paramList: paramDecl (SEMI paramDecl)*;

paramDecl: variableList COLON mptype;

mptype: INTmptype | BOOLmptype | REALmptype | STRINGmptype | array;

array: ARRAY LSB INTLIT DOUDOT INTLIT RSB OF (REALmptype | BOOLmptype | INTmptype | STRINGmptype);

// Expression
expression: andExp;

andExp:
      andExp AND THEN conditionExp
    | andExp OR ELSE  conditionExp
    | conditionExp
    ;

conditionExp:
      addExp EQUAL addExp
    | addExp NOTEQUAL addExp
    | addExp LESSTHAN addExp
    | addExp LESSTHANOREQUAL addExp
    | addExp GREATERTHAN addExp
    | addExp GREATERTHANOREQUAL addExp
    | addExp
    ;

addExp:
      addExp ADD mulExp
    | addExp SUB mulExp
    | addExp OR mulExp
    | mulExp
    ;

mulExp:
      mulExp DIV notExp
    | mulExp MUL notExp
    | mulExp INTDIV notExp
    | mulExp MOD notExp
    | mulExp AND notExp
    | notExp
    ;

notExp:
      SUB notExp
    | NOT notExp
    | indexExp
    ;

indexExp:
      primaryExp LSB expression RSB
    | primaryExp
    ;

primaryExp:
      LB expression RB
    | operand
    ;

operand:
        literal
      | ID
      | functionCall
      ;

functionCall: ID LB expressionList? RB;

expressionList: expression (CM expression)*;

literal: BOOLLIT | INTLIT | FLOATLIT | STRINGLIT;

statement:
      assignStmt
    | ifStmt
    | whileStmt
    | forStmt
    | breakStmt
    | continueStmt
    | returnStmt
    | compoundStmt
    | withStmt
    | callStmt
    ;

assignStmt: leftSide (':=') expression SEMI;

leftSide: member ((':=') member)*;

member:
      ID
    | arrayEle
    | indexExp
    ;

arrayEle: ID LSB INTLIT RSB;

ifStmt: IF LB expression RB statement (ELSE statement)?;

whileStmt: WHILE expression DO statement;

forStmt: FOR ID ':=' expression (TO | DOWNTO) expression DO statement;

breakStmt: BREAK SEMI;

continueStmt: CONTINUE SEMI;

returnStmt: RETURN expression? SEMI;

compoundStmt: BEGIN (statement)* END;

withStmt: WITH variableDeclarationList DO statement;

variableDeclarationList: (variableList COLON mptype SEMI)+;

callStmt: ID LB expressionList? RB SEMI;

// Keywords
BREAK: ('b'|'B') ('r'|'R') ('e'|'E') ('a'|'A') ('k'|'K');
CONTINUE: ('c'|'C') ('o'|'O') ('n'|'N') ('t'|'T') ('i'|'I') ('n'|'N') ('u'|'U') ('e'|'E');
FOR: ('f'|'F') ('o'|'O') ('r'|'R');
TO: ('t'|'T')('o'|'O');
DOWNTO: ('d'|'D') ('o'|'O') ('w'|'W') ('n'|'N') ('t'|'T') ('o'|'O');
DO: ('d'|'D') ('o'|'O');
IF: ('i'|'I') ('f'|'F');
THEN: ('t'|'T') ('h'|'H') ('e'|'E') ('n'|'N');
ELSE: ('e'|'E') ('l'|'L') ('s'|'S') ('e'|'E');
RETURN: ('r'|'R') ('e'|'E') ('t'|'T') ('u'|'U') ('r'|'R') ('n'|'N');
WHILE: ('w'|'W') ('h'|'H') ('i'|'I') ('l'|'L') ('e'|'E');
BEGIN: ('b'|'B') ('e'|'E') ('g'|'G') ('i'|'I') ('n'|'N');
END: ('e'|'E') ('n'|'N') ('d'|'D');
FUNCTION: ('f'|'F') ('u'|'U') ('n'|'N') ('c'|'C') ('t'|'T') ('i'|'I') ('o'|'O') ('n'|'N');
PROCEDURE: ('p'|'P') ('r'|'R') ('o'|'O') ('c'|'C') ('e'|'E') ('d'|'D') ('u'|'U') ('r'|'R') ('e'|'E');
VAR: ('v'|'V') ('a'|'A') ('r'|'R');
ARRAY: ('a'|'A') ('r'|'R') ('r'|'R') ('a'|'A') ('y'|'Y');
OF: ('o'|'O') ('f'|'F');
WITH: ( 'w'|'W') ('i'|'I') ('t'|'T') ('h'|'H');
REALmptype: ('r'|'R') ('e'|'E') ('a'|'A') ('l'|'L') ;
BOOLmptype: ('b'|'B') ('o'|'O') ('o'|'O') ('l'|'L') ('e'|'E') ('a'|'A')  ('n'|'N');
INTmptype: ('i'|'I') ('n'|'N') ('t'|'T') ('e'|'E') ('g'|'G') ('e'|'E') ('r'|'R');
STRINGmptype: ('s'|'S') ('t'|'T') ('r'|'R') ('i'|'I') ('n'|'N') ('g'|'G');

// Identifiers
ID: [a-zA-Z_][a-zA-Z_0-9]* ;

// Separators
LSB: '[' ;

RSB: ']' ;

COLON: ':';

LB: '(';

RB: ')';

SEMI: ';';

DOUDOT: '..';

CM: ',';

// Literals
INTLIT: [0-9]+;

// float-point literal
fragment DECIMAL_FLOAT:[0-9]*'.'[0-9]+ | [0-9]+'.'[0-9]*;
fragment EXPONENT_FLOAT: (DECIMAL_FLOAT|[0-9]+)[eE][-]?[0-9]+;
FLOATLIT: DECIMAL_FLOAT | EXPONENT_FLOAT;
BOOLLIT: (('t'|'T') ('r'|'R') ('u'|'U') ('e'|'E')) | (('f'|'F') ('a'|'A') ('l'|'L') ('s'|'S') ('e'|'E'));

// string literal
STRINGLIT:'"'(~[\n"\\] | ESCAPESTRING)*'"' {self.text = self.text[1:len(self.text)-1]};

fragment ESCAPESTRING: '\\'[bfrnt'"\\];//"""



// Operator
ADD: '+';
SUB: '-';
MUL: '*';
DIV: '/';
NOT: ('n'|'N') ('o'|'O') ('t'|'T');
MOD: ('m'|'M') ('o'|'O') ('d'|'D');
OR: ('o'|'O') ('r'|'R');
AND: ('a'|'A') ('n'|'N') ('d'|'D');
NOTEQUAL: '<>';
EQUAL: '=';
LESSTHAN: '<';
GREATERTHAN: '>';
LESSTHANOREQUAL: '<=';
GREATERTHANOREQUAL: '>=';
INTDIV: ('d'|'D') ('i'|'I') ('v'|'V');

// Comment
LINECOMMENT: '//' ~[\n]* -> skip;
BLOCKCOMMENT: (('(*' .*? '*)') | ('{' .*? '*}')) -> skip;

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines

ILLEGAL_ESCAPE: '"'(~[\n"\\] | ESCAPESTRING)*?'\\'~[bfrnt'"\\] {raise IllegalEscape(self.text[1:])};//"
UNCLOSE_STRING: '"'(~[\n"\\] | ESCAPESTRING)* {raise UncloseString(self.text[1:])};//"
ERROR_CHAR: . {raise ErrorToken(self.text)};