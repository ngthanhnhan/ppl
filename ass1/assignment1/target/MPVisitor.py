# Generated from main/mp/parser/MP.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .MPParser import MPParser
else:
    from MPParser import MPParser

# This class defines a complete generic visitor for a parse tree produced by MPParser.

class MPVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by MPParser#program.
    def visitProgram(self, ctx:MPParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#declaration.
    def visitDeclaration(self, ctx:MPParser.DeclarationContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#variableDeclaration.
    def visitVariableDeclaration(self, ctx:MPParser.VariableDeclarationContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#variableList.
    def visitVariableList(self, ctx:MPParser.VariableListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#functionDeclaration.
    def visitFunctionDeclaration(self, ctx:MPParser.FunctionDeclarationContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#procedureDeclaration.
    def visitProcedureDeclaration(self, ctx:MPParser.ProcedureDeclarationContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#paramList.
    def visitParamList(self, ctx:MPParser.ParamListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#paramDecl.
    def visitParamDecl(self, ctx:MPParser.ParamDeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#mptype.
    def visitMptype(self, ctx:MPParser.MptypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#array.
    def visitArray(self, ctx:MPParser.ArrayContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exp.
    def visitExp(self, ctx:MPParser.ExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#andExp.
    def visitAndExp(self, ctx:MPParser.AndExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#conditionExp.
    def visitConditionExp(self, ctx:MPParser.ConditionExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#addExp.
    def visitAddExp(self, ctx:MPParser.AddExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#mulExp.
    def visitMulExp(self, ctx:MPParser.MulExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#notExp.
    def visitNotExp(self, ctx:MPParser.NotExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#indexExp.
    def visitIndexExp(self, ctx:MPParser.IndexExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#primaryExp.
    def visitPrimaryExp(self, ctx:MPParser.PrimaryExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#operand.
    def visitOperand(self, ctx:MPParser.OperandContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#functionCall.
    def visitFunctionCall(self, ctx:MPParser.FunctionCallContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expressionList.
    def visitExpressionList(self, ctx:MPParser.ExpressionListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#literal.
    def visitLiteral(self, ctx:MPParser.LiteralContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#stmt.
    def visitStmt(self, ctx:MPParser.StmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#assignStmt.
    def visitAssignStmt(self, ctx:MPParser.AssignStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#leftSide.
    def visitLeftSide(self, ctx:MPParser.LeftSideContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#member.
    def visitMember(self, ctx:MPParser.MemberContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#arrayEle.
    def visitArrayEle(self, ctx:MPParser.ArrayEleContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#ifStmt.
    def visitIfStmt(self, ctx:MPParser.IfStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#whileStmt.
    def visitWhileStmt(self, ctx:MPParser.WhileStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#forStmt.
    def visitForStmt(self, ctx:MPParser.ForStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#breakStmt.
    def visitBreakStmt(self, ctx:MPParser.BreakStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#continueStmt.
    def visitContinueStmt(self, ctx:MPParser.ContinueStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#returnStmt.
    def visitReturnStmt(self, ctx:MPParser.ReturnStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#compoundStmt.
    def visitCompoundStmt(self, ctx:MPParser.CompoundStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#withStmt.
    def visitWithStmt(self, ctx:MPParser.WithStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#variableDeclarationList.
    def visitVariableDeclarationList(self, ctx:MPParser.VariableDeclarationListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#callStmt.
    def visitCallStmt(self, ctx:MPParser.CallStmtContext):
        return self.visitChildren(ctx)



del MPParser